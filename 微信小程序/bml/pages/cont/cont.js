//index.js
//获取应用实例
var app = getApp()
Page( {
  data: {
    markers: [{
      iconPath: "https://xcxweb.ganzhouweixin.cn/images/mp.png",
      id: 0,
      latitude: 27.831050,
      longitude: 113.084646,
      width: 50,
      height: 50
    }],
    polyline: [{
      points: [{
        longitude: 113.084646,
        latitude: 27.831050
      }, {
          longitude: 113.084646,
          latitude: 27.831050
      }],
      color: "#FF0000DD",
      width: 2,
      dottedLine: true
    }],
    
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    // tab切换
    currentTab: 0,
  },
  regionchange(e) {
    console.log(e.type)
  },
  markertap(e) {
    console.log(e.markerId)
  },
  controltap(e) {
    console.log(e.controlId)
  },
  calling: function (e) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.mobile, //此号码并非真实电话号码，仅用于测试
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  onLoad: function() {
    var that = this;

    /**
     * 获取系统信息
     */
    wx.getSystemInfo( {

      success: function( res ) {
        that.setData( {
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }

    });
  },
  /**
     * 滑动切换tab
     */
  bindChange: function( e ) {

    var that = this;
    that.setData( { currentTab: e.detail.current });

  },
  /**
   * 点击tab切换
   */
  swichNav: function( e ) {

    var that = this;

    if( this.data.currentTab === e.target.dataset.current ) {
      return false;
    } else {
      that.setData( {
        currentTab: e.target.dataset.current
      })
    }
  }
})