// news.js
//获取应用实例
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    hidden: true,
    scrollTop: 0,
    scrollHeight: 0,
    page: 1,
    isAjax: true,
    isGet: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          clientHeight: res.windowHeight,
          scrollHeight: res.windowHeight,
          parentId: options.parentId
        });
      }
    });
    this.loadMore();
  },
  //页面滑动到底部
  bindDownLoad: function () {
    if (this.data.isAjax && this.data.isGet) {
      this.loadMore();
    }
    console.log("123");
  },
  loadMore: function () {
    var that = this;
    this.setData({
      hidden: false,
      isGet: false
    });
    app.ajaxRequest({
      url: app.url,
      data: {
        opt: 'getArticle',
        parentId: that.data.parentId,
        page: that.data.page++,
        pageSize:6
      },
      success: function (res) {
        console.info(res.data.ds);
        var list = that.data.listData;
        if (res.data.status == 0) {
          for (var i = 0; i < res.data.ds.length; i++) {
            list.push(res.data.ds[i]);
          }
          that.setData({
            listData: list,
            hidden: true,
            isGet: true
          });
          if (res.data.ds.length < 3) {
            that.data.isAjax = false;
          }
        }
        else {
          that.setData({
            hidden: true,
            isGet: false
          });
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})