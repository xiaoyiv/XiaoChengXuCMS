//index.js
//获取应用实例
var app = getApp()
Page( {
  data: {
    about: [],
    gybml:[],
    banner_text:[
      {
        "title": "我们是谁",
        "subtitle": "WHO ARE WE"
      }
    ],
    onReady: function (res) {
    this.videoContext = wx.createVideoContext('myVideo')
  },
  inputValue: '',
    data: {
        src: '',
    },
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    // tab切换
    currentTab: 0,
  },
  onLoad: function (options) {
    var that = this;

    /**
     * 获取系统信息
     */
    wx.getSystemInfo( {

      success: function( res ) {
        that.setData( {
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          clientHeight: res.windowHeight,
          currentTab: options.tab
        });
      }

    });
    wx.request({
      url: app.url, //接口地址
      data: {
        opt: 'getAbout',
        cloumnId: '124',
        cloumnName: 'about'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          about: res.data.about,
        })
      }
    });
    wx.request({
      url: app.url, //接口地址
      data: {
        opt: 'getAbout',
        cloumnId: '123',
        cloumnName: 'gybml'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          gybml: res.data.gybml,
        })
      }
    });
    wx.request({
      url: app.url, //接口地址
      data: {
        opt: 'getAbout',
        cloumnId: '125',
        cloumnName: 'cply'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          cply: res.data.cply,
        })
      }
    });
    wx.request({
      url: app.url, //接口地址
      data: {
        opt: 'getCply'
        
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          dsCply: res.data.ds,
        })
      }
    });
  },
  /**
     * 滑动切换tab
     */
  bindChange: function( e ) {

    var that = this;
    that.setData( { currentTab: e.detail.current });

  },
  /**
   * 点击tab切换
   */
  swichNav: function( e ) {

    var that = this;

    if( this.data.currentTab === e.target.dataset.current ) {
      return false;
    } else {
      that.setData( {
        currentTab: e.target.dataset.current
      })
    }
    
  }
})