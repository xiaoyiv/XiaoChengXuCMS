//index.js
//获取应用实例
var app = getApp()

Page({
  formSubmit: function (e) { 
    var that = this;
    if (e.detail.value.mobile.length == 0 || e.detail.value.txtname.length == 0 || e.detail.value.desc.length == 0) {
      wx.showToast({
        title: '输入不得为空!',
        icon: 'loading',
        duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
      }, 2000)
    } else if (e.detail.value.mobile.length != 11) {
      wx.showToast({
        title: '请输入11位手机号码!',
        icon: 'loading',
        duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
      }, 2000)
    } else {
    wx.request({
      url: app.url, //接口地址
      data: {
        opt: 'postMessage',
        txtname: e.detail.value.txtname,
        mobile: e.detail.value.mobile,
        desc: e.detail.value.desc
      },
      header: {
        'content-type': 'application/json'
      },   
      success: function (res) {
        if (res.data.status == 0) {    
          that.setData({
            txtname: '',
            mobile: '',
            desc: ''
          })      
          wx.showToast({
            title: '提交成功!',//这里打印出登录成功
            icon: 'success',
            duration: 1000
          })
        } else {         
          wx.showToast({
            title: '提交失败!',
            icon: 'loading',
            duration: 1500
          })
        }
      }
    })
    }
  },
  formReset: function () {
    console.log('form发生了reset事件')
  },
  calling: function (e) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.mobile, //此号码并非真实电话号码，仅用于测试
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  data: {
     case_title:[
      {
        "title": "案例视频",
        "subtitle": "CASE VIDEO"
      }
    ],
     txtname: '',
     mobile: '',
     desc: '',
     duration: 2000,
     indicatorDots: true,
     autoplay: true,
     interval: 3000,
     loading: false,
     plain: false
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    console.log('onLoad')
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    });
    wx.request({//banner图
      url: app.url, //接口地址
      data: {
        opt: 'getBanner'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          imgUrls: res.data.ds,        
        })
      }
    });
    wx.request({//产品中心
      url: app.url, //接口地址
      data: {
        opt: 'getProduct',
        parentId: 94
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          product: res.data.ds,
        })
      }
    });
    wx.request({//用户案例
      url: app.url, //接口地址
      data: {
        opt: 'getUserCase'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          userCase: res.data.ds,
        })
      }
    });
    wx.request({//案例视频
      url: app.url, //接口地址
      data: {
        opt: 'getCaseVideo'
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          caseVideo: res.data.ds,
        })
      }
    });
    wx.request({//地区代理
      url: app.url, //接口地址
      data: {
        opt: 'getArticle',
        parentId: 127
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          proxy: res.data.ds,
        })
      }
    });
  }
})
