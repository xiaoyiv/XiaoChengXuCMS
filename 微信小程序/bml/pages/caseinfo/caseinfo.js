import WxParse from '../../wxParse/wxParse.js';
//pro.js
//获取应用实例
var app = getApp();

Page({
  data: {
    proinfoimgUrls: [],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    // tab切换
    currentTab: 0,
    webUrl: app.webUrl,
  },
  onLoad: function (options) {
    var that = this;

    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          clientHeight: res.windowHeight
        });
      }

    });
    
    wx.request({//文章详情
      url: app.url, //接口地址
      data: {
        opt: 'getArticleDe',
        cloumnName: 'caseinfo_item',
        id: options.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          caseinfo_item: res.data.ds,
          content: WxParse.wxParse('article_content', 'html', res.data.ds[0].content, that)
        })
      }
    });
    wx.request({//文章详情上一篇
      url: app.url, //接口地址
      data: {
        opt: 'getPre',
        id: options.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          pre_item: res.data.ds,
          
        })
      }
    });
    wx.request({//文章详情下一篇
      url: app.url, //接口地址
      data: {
        opt: 'getNext',
        id: options.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          next_item: res.data.ds,
         
        })
      }
    });
   
  },

  /**
     * 滑动切换tab
     */
  bindChange: function (e) {

    var that = this;
    that.setData({ currentTab: e.detail.current });

  },
  /**
   * 点击tab切换
   */
  swichNav: function (e) {

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  }
})


