﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cms.Common;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;
/// <summary>
///ToAspx 的摘要说明
/// </summary>
public class ToAspx
{
	public ToAspx()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    #region 返回栏目url=========================================
    public static string getCloumnUrl(int id)
    {
        string result = "";
        Cms.Model.C_Column model = new Cms.BLL.C_Column().GetModel(id);
       
        if (model != null)
        {
            if (model.modelId == 1)
            {
                DataSet ds = new Cms.BLL.C_Column().GetList(1, "parentId=" + id, "classId asc");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    result = "/html/" +ds.Tables[0].Rows[0]["tpl_channel"].ToString() + ".aspx?id=" + ds.Tables[0].Rows[0]["classId"].ToString();
                }
            }
            else
            {
                result = "/html/" + model.tplChannel + ".aspx?id=" + id;
            }
        }
        return result;
    }
   
    #endregion

    #region 返回栏目的内容Url==========================================
    public static string getContentUrl(int id, int articleId)
    {
        string result = "";
        Cms.Model.C_Column model = new Cms.BLL.C_Column().GetModel(id);

        if (model != null)
        {
            result = "/html/" + model.tplContent + ".aspx?articleId=" + articleId;
        }
        return result;
    }

    #endregion

    #region 返回栏目当前焦点=========================================
    public static string getActive(string id, string articleId,int classId)
    {
        string result = "";
        if (id != null&&articleId==null)
        {
            if (Convert.ToInt32(id) == classId)
            {
                result = "active";
            }
            else
            {
                Cms.Model.C_Column model = new Cms.BLL.C_Column().GetModel(Convert.ToInt32(id));
                if (model.parentId == classId)
                {
                    result = "active";
                }
            }
        }
        else
        {
            Cms.Model.C_article model = new Cms.BLL.C_article().GetModel(Convert.ToInt32(articleId));
            if (model != null)
            {
                Cms.Model.C_Column modelC_Column = new Cms.BLL.C_Column().GetModel(Convert.ToInt32(model.parentId));
                if (modelC_Column.classId == classId)
                {
                    result = "active";
                }
                else if ((modelC_Column.parentId == classId))
                {
                    result = "active";
                }
            }
        }
        return result;
    }

    #endregion

    #region 返回栏目名称=======================================
    public static string getCloumnName(int id)
    {
        string result = "";
        Cms.BLL.C_Column bllColumn = new Cms.BLL.C_Column();
        DataSet ds = bllColumn.GetList("classId=" + id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            result = ds.Tables[0].Rows[0]["className"].ToString();
        }
        return result;
    }

    #endregion

    #region 返回栏目缩略图==========================================
    public static string getCloumnPicurl(int id)
    {
        string result = "";
        Cms.BLL.C_Column bllColumn = new Cms.BLL.C_Column();
        DataSet ds = bllColumn.GetList("classId=" + id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            result = ds.Tables[0].Rows[0]["photoUrl"].ToString();
        }
        return result;
    }

    #endregion

    #region 返回栏目简介=================================
    public static string getCloumnIntro(int id)
    {
        string result = "";
        Cms.BLL.C_Column bllColumn = new Cms.BLL.C_Column();
        DataSet ds = bllColumn.GetList("classId=" + id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            result = GetToHtml(ds.Tables[0].Rows[0]["intro"].ToString());
        }
        return result;
    }

    #endregion

    #region 返回栏目内容===================================
    public static string getCloumncontent(int id)
    {
        string result = "";
        Cms.BLL.C_Column bllColumn = new Cms.BLL.C_Column();
        DataSet ds = bllColumn.GetList("classId=" + id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            result = GetToHtml(ds.Tables[0].Rows[0]["content"].ToString());
        }
        return result;
    }

    #endregion

   

    #region 返回通过文章id返回栏目的内容链接=======================
    public static string getContentUrlsub(int id)
    {
        string result = "";
        Cms.BLL.C_Column bllColumn = new Cms.BLL.C_Column();
        Cms.BLL.C_article bllarticle = new Cms.BLL.C_article();
        int parentid =Convert.ToInt32(bllarticle.GetModel(id).parentId);
        DataSet ds = bllColumn.GetList("classId=" + parentid);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            result = GetToHtml(ds.Tables[0].Rows[0]["contentUrl"].ToString());
        }
        return result;
    }

    #endregion

    #region 去除html==============================================
    public static string GetToHtml(string intro)
    {
        //删除脚本   
        intro = Regex.Replace(intro, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
        //删除HTML   
        intro = Regex.Replace(intro, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"-->", "", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"<!--.*", "", RegexOptions.IgnoreCase);

        intro = Regex.Replace(intro, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(nbsp|#160);", "   ", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
        intro = Regex.Replace(intro, @"&#(\d+);", "", RegexOptions.IgnoreCase);

        intro.Replace("<", "");
        intro.Replace(">", "");
        intro.Replace("\r\n", "");
        intro = HttpContext.Current.Server.HtmlEncode(intro).Trim();

        return intro;
    }
     #endregion

    #region 截取字符长度======================================
    public static string getLength(string str, int n)
    {
        if (str.Length > n)
            return str.Substring(0, n) + "...";
        else
            return str;
    }
    public static string getLengthNo(string str, int n)
    {
        if (str.Length > n)
            return str.Substring(0, n);
        else
            return str;
    }
    #endregion

    #region 去除HTML标记======================================== 
    public static string NoHTML(string Htmlstring)
    {
        //删除脚本   
        Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
        //删除HTML   
        Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);

        // Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", "   ", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
        Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);

        Htmlstring.Replace("<", "");
        Htmlstring.Replace(">", "");
        Htmlstring.Replace("\r\n", "");
        Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();

        return Htmlstring;
    }
    #endregion

    #region 遍历栏目id============================================
    public static string getcloumnid(int classid)
    {
        string result = "";
        Cms.BLL.C_Column bll = new Cms.BLL.C_Column();
        DataSet ds = bll.GetList("parentId=" + classid);
        StringBuilder sbuBuilder = new StringBuilder();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                sbuBuilder.Append(ds.Tables[0].Rows[i]["classId"].ToString() + ",");
            }
            if (sbuBuilder.Length > 1)
            {
                sbuBuilder.Remove(sbuBuilder.Length - 1, 1);
            }
            result = sbuBuilder.ToString();
        }
        else
        {
            result = classid.ToString();
        }
      
        return result;
    }
    #endregion
}