﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="msg.aspx.cs" Inherits="wx_msg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>微客服</title>
    <meta charset="utf-8">
    <meta name="description">
    <meta name="keywords">
    <meta content="eric.wu" name="author">
    <meta content="no-cache,must-revalidate" http-equiv="Cache-Control">
    <meta content="no-cache" http-equiv="pragma">
    <meta content="0" http-equiv="expires">
    <meta content="telephone=no, address=no" name="format-detection">
    <meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <link rel="stylesheet" type="text/css" href="js/mobilemain.css">
    <script src="js/jquery_min.js"></script>
    <script src="js/jquery-1.3.2.min.js"></script>
    <script>
        document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
            WeixinJSBridge.call('hideToolbar');
            WeixinJSBridge.call('hideOptionMenu');
            $(window).scrollTop(document.body.scrollHeight);
        });
    </script>
    <script type="text/javascript">

        function LoadMsg() {
           
            var SendUserName = $("#openid").val();
           
            $.ajax({
                type: "POST",
                url: "ashx/GetMsg.ashx?type=GetMsg&SendUserName=" + SendUserName,
                data: "",
                success: function (msg) {
                    $("#containertop").html(msg);
                    $("#containertop").scrollTop(500);
                }
            });
        }

        $(function () {
            setInterval("LoadMsg()", 1000);
        });

    </script>
</head>
<body onselectstart="return true;" ondragstart="return false;">
    <div id="container" class="container animate">
        <div class="containertop" id="containertop">   
                    
        </div>
        <footer>
            <section class="nav_footer" id="nav_footer">
                <ul>
                    <ol class="tbox">
                        <li>
                            <a class="pointer toolsface" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" id="facePoint"></a>
                        </li>

                        <li></li>
                        
                        <li style="width: 100%;">
                            <input type="text" id="sendtext" oninput="cansend()" onpropertychange="cansend()" class="toolstext" style="width: 100%;">
                        </li>

                        <li>
                            <a id="btnsend" class="toolssend on" style="height: 24px; padding-top: 6px; margin: 0 7px;">发送</a>
                             <input type="hidden" id="openid" value="<%=xtopenid %>" runat="server" />                              
                        </li>
                    </ol>
                </ul>
            </section>
        </footer>
    </div>
    <script type="text/javascript" src="js/emotion.js"></script>
    <script type="text/javascript" src="js/helper_min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/MobileCustomer.js"></script>
    <script type="text/javascript">
        var userAgent = navigator.userAgent.toLowerCase();
        var IsIOS = false;
        if (userAgent.indexOf("iphone") >= 0) {
            IsIOS = true;
            $("#nav_footer ul").css("position", "absolute");
        }

        function cansend() {
            var text = $("#sendtext").val();
            var btn = $("#btnsend");
            if (text.trim().length > 0) {
                btn.removeClass("on");
            }
            else {
                btn.addClass("on");
            }
        }

        $("#btnsend").click(function () {
            var text = $("#sendtext").val();
            if (text.trim().length == 0) {
                return;
            }
            else {
                SendMsg()
            }
        });



        function SendMsg() {
           
            var openid = $("#openid").val();
           
            var msg = encodeURI(document.getElementById("sendtext").value);
            $.ajax({
                type: "POST",
                url: "ashx/GetMsg.ashx?type=SendMsg&Msg=" + msg + "&Openid=" + openid,
                data: "",
                success: function (msg) {
                   
                    LoadMsg();
                    document.getElementById("sendtext").value = "";
                }
            });
        }

        window.preViewImg = (function () {
            var imgsSrc = {};
            function reviewImage(dsrc, gid) {
                if (typeof window.WeixinJSBridge != 'undefined') {
                    WeixinJSBridge.invoke('imagePreview', {
                        'current': dsrc,
                        'urls': imgsSrc[gid]
                    });
                } else {
                    alert("请在微信中查看", null, function () { });
                }
            }
            function init(thi, evt) {
                var dsrc = thi.getAttribute("data-src");
                var gid = thi.getAttribute("data-gid");

                if (dsrc && gid) {
                    imgsSrc[gid] = imgsSrc[gid] || [];
                    imgsSrc[gid].push(dsrc);
                    thi.addEventListener("click", function () {
                        reviewImage(dsrc, gid);
                    }, false);
                }
            }
            return init;
        })();


</script>
</body>
</html>
