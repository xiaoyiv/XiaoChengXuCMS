﻿<%@ WebHandler Language="C#" Class="GetMsg" %>

using System;
using System.Web;
using System.Data;
using System.Text;
public class GetMsg : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/html";
        string type = context.Request.QueryString["type"];
        string SendUserName = context.Request.QueryString["SendUserName"];
        string Msg = context.Request.QueryString["Msg"];
        string Openid = context.Request.QueryString["Openid"];
        Cms.BLL.XT_ChatLog chatlog = new Cms.BLL.XT_ChatLog();
        Cms.Model.XT_ChatLog chatloginfo = new Cms.Model.XT_ChatLog();
        Cms.BLL.wx_info info = new Cms.BLL.wx_info();

      
        string wxid = info.GetModel(1).wxid;
        Cms.Model.XT_ChatLog[] cl = chatlog.GetChatLogList(wxid, SendUserName);
        Cms.Model.XT_ChatLog[] cllist = chatlog.GetUserList();
        string str = "";
       
        switch (type)
        {
            case "GetMsg":
               
                for (int i = 0; i < cl.Length; i++)
                {
                    if (cl[i].SendUserName != null)
                    {
                        string NickName = "";

                        if (cl[i].SendUserName == wxid)
                        {
                            NickName = "系统";
                        }
                        else 
                        {
                            NickName = wxuser.getwxuserinfo(cl[i].SendUserName).nickname.ToString();
                            if (NickName == "")
                            {
                                NickName = "未知";
                            }
                        }

                        if (NickName == "系统")
                        {
                            str += "<p class=\"time\" style=\"display: block;\">" + Convert.ToDateTime(cl[i].CreateTime).ToString("yyyy/MM/dd  hh:mm:ss") + "</p>";
                            str += "<ul class=\"ul_talk\">";
                            str += "<li class=\"tbox\">";
                            str += "<div><span class=\"head\"><img src=\"js/mengmei.jpg\"></span><label class=\"name\">系统</label></div>";
                            str += "<div><span class=\"arrow\"><svg><path d=\"M2,20 A9,5,0,0,1,18,24 L20,0\" stroke-width=\"1\" stroke=\"#e2e2e2\" fill=\"#e2e2e2\"></path></svg></span></div>";
                            str += " <div><article class=\"content\">" + cl[i].MsgContent + "</article></div>";
                            str += " </li>";
                            str += " </ul>";  
                        }
                        else
                        {
                            str += "<p class=\"time\" style=\"display: block;\">" + Convert.ToDateTime(cl[i].CreateTime).ToString("yyyy/MM/dd  hh:mm:ss") + "</p>";
                            str += "<ul class=\"ul_talk reply\">";
                            str += "<li class=\"tbox\"><div><span class=\"head\">";
                            str += "<img src=\"" + wxuser.getwxuserinfo(cl[i].SendUserName).headimgurl + "\"></span></div>";
                            str += "<div><span class=\"arrow\"><svg><path d=\"M18,40 A9,5,0,0,0,2,37 L0,23\" stroke-width=\"1\" stroke=\"#2792ff\" fill=\"#2792ff\"></path></svg></span></div>";
                            str += "<div><article class=\"content\">" + cl[i].MsgContent + "</article></div></li>";
                            str += " </ul>";
                        }
                        
                    }
                }

                break;
 
            case "SendMsg":
              
                chatloginfo.FromUserName = wxid;
                chatloginfo.ToUserName = Openid;
                chatloginfo.SendUserName = Openid;
                chatloginfo.Worker = "0";
                chatloginfo.MsgContent = System.Web.HttpUtility.UrlDecode(Msg);
                chatlog.Add(chatloginfo);             
                break;
                
            default:
                break;
        }

        context.Response.Write(str);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}