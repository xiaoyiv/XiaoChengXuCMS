﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
    public void Bind()
    {
        lbname.Text = Application["phone"].ToString();
    }
    protected void btsave_Click(object sender, EventArgs e)
    {

        if (Application["phone"] == null || Application["phone"].ToString() == "")
        {
            this.Response.Write("<Script Language='JavaScript'>window.alert('注册失败');window.location.href ='UserRegister.aspx'</script>");
            return;
        }
       
        

        Cms.BLL.C_user cuser = new Cms.BLL.C_user();
        Cms.Model.C_user muser = new Cms.Model.C_user();

        string stel = Application["phone"].ToString();
        if (cuser.Exists(stel))
        {
            this.Response.Write("<Script Language='JavaScript'>window.alert('注册失败,该手机号已经注册过！');window.location.href ='UserRegister.aspx'</script>");
            return;
        }

        string usercard = wxuser.getusercard();
        muser.telphone = stel;
        muser.username = tbname.Text.Trim();
        muser.password = tbpwd.Text.Trim();
        muser.isbind = 1;
        muser.isSign = 1;
        muser.userscore = 0;
        muser.userMoney = 0;
        if (wxuser.isExistsUsercard(usercard))
        {
            muser.usercard = wxuser.getusercard();
        }
        else {
            muser.usercard = usercard;
        }
      
        muser.updatetime = DateTime.Now;
        if (cuser.Add(muser) > 0)
        {
            this.Response.Write("<Script Language='JavaScript'>window.alert('提交成功');window.location.href ='UserRegister_step.aspx'</script>");
        }
        else {
            this.Response.Write("<Script Language='JavaScript'>window.alert('提交失败');window.location.href ='UserRegister_step.aspx'</script>");
        }

    }
}