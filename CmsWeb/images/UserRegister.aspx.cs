﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btsave_Click(object sender, EventArgs e)
    {
        Cms.BLL.C_code code = new Cms.BLL.C_code();
        string sphone = tbphone.Text.Trim();
        string scode = tbcode.Text.Trim();
        bool bl = false;
        if (code.getcode(sphone, scode))
        {
            if (code.setcode(sphone))
            {
                bl = true;
            }
        }
        if (bl)
        {
            Application["phone"] = tbphone.Text.Trim();
            Response.Redirect("UserRegister_step.aspx");
        }
        else {
            this.Response.Write("<Script Language='JavaScript'>window.alert('请重新获取验证码！');window.location.href ='UserRegister.aspx'</script>");
        }
    }
    protected void btcode_Click(object sender, EventArgs e)
    {
        string smsg = ToWap.sendSMS(tbphone.Text.Trim());
        Response.Write(smsg);
    }
}