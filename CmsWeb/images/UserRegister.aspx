﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRegister.aspx.cs" Inherits="UserRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<script>
    function ck() {
        var ckbox = document.getElementById("ckbox");
        var ckboxs = document.getElementById("tbphone").value;
        if (ckbox.checked && ckboxs == '输入手机号码' && ckboxs.length > 0) {
            return true;
        } else {
            alert('请同意《DBE会员章程》');
           return false;
        }
   }

   function ckphone() {
       var ckbox = document.getElementById("tbphone").value;
       if (ckbox.length > 0 && ckbox == '输入手机号码') {
           return true;
       } else {
           alert('输入手机号码！');
           return false;
       }
   }
</script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr><th>开通新会员</th></tr>
    <tr>
    <td><asp:TextBox ID="tbphone" runat="server" Text="输入手机号码" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"  onfocus="if (value =='输入手机号码'){value =''}" onblur="if  (value ==''){value='输入手机号码'}"></asp:TextBox></td>
    </tr>
    <tr>
    <td><asp:TextBox ID="tbcode" runat="server" Text="输入验证码"  onfocus="if (value =='输入验证码'){value =''}" onblur="if  (value ==''){value='输入验证码'}"></asp:TextBox>
        <asp:Button
            ID="btcode" runat="server" Text="获取验证码" onclick="btcode_Click" OnClientClick="return ckphone()" /></td>
    </tr>
     <tr>   
    <td><input id="ckbox" type="checkbox" />同意《DBE会员章程》</td>
    </tr>
     <tr>   
    <td><asp:Button ID="btsave" runat="server" Text="下一步" onclick="btsave_Click" OnClientClick="return ck()"  /></td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
