﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="百度地图,百度地图API，百度地图自定义工具，百度地图所见即所得工具" />
    <meta name="description" content="百度地图API自定义地图，帮助用户在可视化操作下生成百度地图" />
    <title>百度地图API自定义地图</title>
    <!--引用百度地图API-->
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=k1VkeLuUpRVoYpKGD29RQwlq"></script>
  </head>
  
  <body>
  <form id="form1" runat="server">

  <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
  
  <asp:SiteMapPath ID="SiteMapPath1" runat="server">
  </asp:SiteMapPath>
  <asp:SiteMapDataSource ID="SiteMapDataSource1" Runat="server" />
<h2>Using TreeView</h2>
<asp:TreeView ID="TreeView1" Runat="Server" DataSourceID="SiteMapDataSource1" />
<h2>Using Menu</h2>
<asp:Menu ID="Menu2" Runat="server" DataSourceID="SiteMapDataSource1"/>
<h2>Using a Horizontal Menu</h2>
<asp:Menu ID="Menu1" Runat="server" DataSourceID="SiteMapDataSource1"
Orientation="Horizontal" StaticDisplayLevels="2" />
    <!--百度地图容器 http://api.map.baidu.com/lbsapi/createmap/images/icon.png -->
    <div style="width:450px;height:150px;border:#ccc solid 1px;font-size:12px" id="map"></div>
    <p style="color:red;font-weight:600">地图生成工具基于百度地图JS api v2.0版本开发，使用请申请密匙。
      <a href="http://developer.baidu.com/map/index.php?title=jspopular/guide/introduction" style="color:#2f83c7" target="_blank">了解如何申请密匙</a>
      <a href="http://lbsyun.baidu.com/apiconsole/key?application=key" style="color:#2f83c7" target="_blank">申请密匙</a>
    </p>
 
      <asp:HiddenField ID="hflat" runat="server" Value="28.213478" />
      <asp:HiddenField ID="hflng" runat="server" Value="112.979353" />

      </form> 
</body>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap() {
        createMap(); //创建地图
        setMapEvent(); //设置地图事件
        addMapControl(); //向地图添加控件
        addMapOverlay(); //向地图添加覆盖物
    }
    function createMap() {
        map = new BMap.Map("map");
        map.centerAndZoom(new BMap.Point(112.97046, 28.200793), 17);
    }
    function setMapEvent() {
        map.enableDragging();
    }
    function addClickHandler(target, window) {
        target.addEventListener("click", function () {
            target.openInfoWindow(window);
        });
    }
    function addMapOverlay() {
        var markers = [
        { content: "橘子洲", title: "长沙", imageOffset: { width: -46, height: -21 }, position: { lat: 28.201111, lng: 112.969256} }
      ];
        for (var index = 0; index < markers.length; index++) {
            var point = new BMap.Point(markers[index].position.lng, markers[index].position.lat);
            var marker = new BMap.Marker(point, { icon: new BMap.Icon("./images/icon.png", new BMap.Size(20, 25), {
                imageOffset: new BMap.Size(markers[index].imageOffset.width, markers[index].imageOffset.height)
            })
            });
            var label = new BMap.Label(markers[index].title, { offset: new BMap.Size(25, 5) });
            var opts = {
                width: 200,
                title: markers[index].title,
                enableMessage: false
            };
            var infoWindow = new BMap.InfoWindow(markers[index].content, opts);
            marker.setLabel(label);
            addClickHandler(marker, infoWindow);
            map.addOverlay(marker);
        };
    }
    //向地图添加控件
    function addMapControl() {
//        var navControl = new BMap.NavigationControl({ anchor: BMAP_ANCHOR_TOP_LEFT, type: BMAP_NAVIGATION_CONTROL_LARGE });
//        map.addControl(navControl);
    }
    var map;
    initMap();
  </script>
</html>
