﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditAddress.aspx.cs" Inherits="shop_EditAddress" ValidateRequest="false" EnableEventValidation="false" EnableViewState="true" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="/shop/user.aspx" class="page-title">收货地址</a>
    </header>
    <section class="main  main-product">
        


        <div class="add-address">
            <ul>
                <li>
                    <span>收货人</span>
                    <div><input name="" type="text" id="consignee" runat="server" class="ipt1"/></div>
                </li>
                <li>
                    <span>手机号</span>
                    <div> <input name="" type="text" id="cellphone" runat="server" /></div>
                </li>
                <li>
                    <span>邮政编码</span>
                    <div><input name="" type="text" id="code" runat="server"/></div>
                </li>
            </ul>
            <ul class="mt10">
                <li>
                    <span>省份</span>
                    <div>
                    	  <asp:DropDownList ID="Province" runat="server" datatype="*" sucmsg=" " AutoPostBack="True"
                        OnSelectedIndexChanged="Province_SelectedIndexChanged">
                    </asp:DropDownList>
                    </div>
                </li>
                <li>
                    <span>城市</span>
                    <div>
                    	<asp:DropDownList ID="City" runat="server" datatype="*" sucmsg=" " AutoPostBack="True"
                        OnSelectedIndexChanged="City_SelectedIndexChanged">
                    </asp:DropDownList>
                    </div>
                </li>
                <li>
                    <span>区/县</span>
                    <div>
                    	<asp:DropDownList ID="District" runat="server" datatype="*" sucmsg=" ">
                    </asp:DropDownList>
                    </div>
                </li>
                <div style=" display:none">
                <input name="" type="text" id="street" runat="server" class="info-input"/></div>
                <li>
                    <span>详细地址</span>
                    <div> <input name="" type="text" id="address" runat="server" /></div>
                </li>
                <li style=" padding-left:80px;">
                    <span>设为默认地址</span>
                    <div>  <div class="rule-multi-radio">
                        <asp:RadioButtonList ID="is_default" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                        </asp:RadioButtonList>
                        </div></div>
                </li>
            </ul>
            
            <div class="address-b bnone">
               <asp:Button ID="Button1" runat="server" CssClass="login_ipt" Text="保存" 
                    onclick="Button1_Click"></asp:Button>
            </div>
        </div>
    </section>
             <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
</section>
    </form>
</body>
</html>
