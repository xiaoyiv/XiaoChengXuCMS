﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class shop_looseList : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected string classIdList;
    DataTable SearchTable = new DataTable();  //声明一个DataTable对象
    DataRow drtable;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();//验证登录
            this.user_id = userModel.id;
            this.BindDate();
        }
    }
    #region 赋值操作========================
    public void BindDate()
    {

        string shape = Request["shape"].ToString();
        string price = Request["price"].ToString();
        string[] pArray = price.Split(';');
        string priceStart = pArray[0];
        string priceEnd = pArray[1];

        string stone = Request["stone"].ToString();
        string[] rArray = stone.Split(';');
        string stoneStart = rArray[0].ToString();
        string stoneEnd = rArray[1].ToString();

        string color = Request["color"].ToString();
        string clarity = Request["clarity"].ToString();
        string cut = Request["cut"].ToString();

        StringBuilder sbuBuilder = new StringBuilder();
        sbuBuilder.Append("and price between " + priceStart + " and " + priceEnd);
        sbuBuilder.Append("and main_stone_weight between " + stoneStart + " and " + stoneEnd);
        if (shape != "")
        {
            sbuBuilder.Append("and diamond_shape like '%" + shape + "%'");
        }
        if (color != "")
        {
            sbuBuilder.Append("and color like '%" + color + "%'");
        }
        if (clarity != "")
        {
            sbuBuilder.Append("and clarity like '%" + clarity + "%'");
        }
        if (cut != "")
        {
            sbuBuilder.Append("and cut like '%" + cut + "%'");
        }
        classIdList = sbuBuilder.ToString();
        DataSet dsProduct = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE parentId =103 " + sbuBuilder + "  ) TT WHERE TT.Row between 0 and 4");
        if (dsProduct != null && dsProduct.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = dsProduct.Tables[0].DefaultView;
            RepList.DataBind();
        }
        else
        {
            RepList.DataSource = dsProduct.Tables[0].DefaultView;
            RepList.DataBind();
        }


    }
    /// <summary>
    /// 价格
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPrice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["price"]).ToString("0.00");
            }
        }
        return result;
    }
    /// <summary>
    /// 钻石重
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getLoose(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["main_stone_weight"].ToString();
            }
        }
        return result;
    }
    /// <summary>
    /// 颜色
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getColor(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["color"].ToString())).title;
            }
        }
        return result;
    }
    /// <summary>
    /// 净度
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getClarity(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["clarity"].ToString())).title;
            }
        }
        return result;
    }
    /// <summary>
    /// 切工
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getCut(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["cut"].ToString())).title;
            }
        }
        return result;
    }
    #endregion
}