﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="shop_about" %>


<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <section class="all">
            <header>
                <a href="javascript:history.back(-1);" class="back"></a>
                <a href="/shop/cart.aspx" class="shopping"></a>
                <a href="javascript:void(0);" class="page-title">公司品牌<%=Application["classId"]%></a>
            </header>
            <section class="main main-product main1">
                <div class="filter">
        	        <ul>
                        <li <%=classId == 117?"class='current'":"" %> ><a href="/shop/about.aspx?id=117">公司简介</a></li>
            	        <li <%=classId == 118?"class='current'":"" %>><a href="/shop/about.aspx?id=118">品牌故事</a></li>
                        <li <%=classId == 119?"class='current'":"" %>><a href="/shop/about.aspx?id=119">品牌文化</a></li>
                        <li <%=classId == 120?"class='current'":"" %>><a href="/shop/about.aspx?id=120">品牌优势</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
                <div class="p-contain">
        	        <div class="p-product-det p-product-dett"><%=Application["ArticleContent"] %></div>
                </div>
                <uc2:footer ID="footer1" runat="server" />
                <uc3:bottom ID="bottom1" runat="server" />
        </section>
        </section>
        <uc1:nav ID="nav1" runat="server" />
    </form>
</body>
</html>
