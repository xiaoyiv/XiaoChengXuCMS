﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productList.aspx.cs" Inherits="shop_productList" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
     <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="/index.aspx" class="nav"></a>
        <a href="/shop/cart.aspx" class="shopping"></a>
        <a href="/shop/productList.aspx" class="page-title">产品中心</a>
    </header>
    <section class="main">
    	<div class="filter">
        	<ul>
            	<li class="<%=Application["type_id"]%>"><a href="/shop/productList.aspx?classid=">经典款</a></li>
                <li class="<%=Application["type_yellow"]%>"><a href="/shop/productList.aspx?type_yellow=101&classid=42">黄金</a></li>
                <li class="<%=Application["type_losse"]%>"><a href="/shop/productList.aspx?type_losse=102&classid=55">镶嵌</a></li>
                <li class="<%=Application["type_price"]%>"><a href="/shop/productList.aspx?classid=56">彩宝</a></li>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="p-contain">
        	<div class="p-product">
            	<ul id="gallery-filtralbe">
                <asp:Repeater runat="server" ID="RepList" OnItemCommand="RepList_ItemCommand">
                <ItemTemplate>
                	<li>
                    	<div class="p-product-con">
                            <a href="/shop/productDetail.aspx?id=<%#Eval("articleId") %>"><img src="<%#Eval("photoUrl") %>" alt=""></a>
                             <asp:HiddenField
                        ID="Fielddocid" Value='<%#Eval("articleId")%>' runat="server" />
                            <div class="p-product-name">
                                <h3><a href="/shop/productDetail.aspx?id=<%#Eval("articleId") %>" title="<%#Eval("title") %>"><%#Eval("title").ToString().Substring(0, 8)%>...</a></h3>
                                <p><span>¥ <%#getPrice(Convert.ToInt32(Eval("articleId"))) %></span><br /><del>原价：¥ <%#getMprice(Convert.ToInt32(Eval("articleId"))) %></del>
                              
                                <asp:LinkButton ID="lbtnIsLess" CommandName="lbtnLess" runat="server" CssClass="p-collect"></asp:LinkButton>
                                </p>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                  <FooterTemplate>
                <%#RepList.Items.Count == 0 ? "<div style='text-align:center;'>暂无产品记录</div>" : ""%>
            </FooterTemplate>
                </asp:Repeater>
                    <div class="clear"></div>
                </ul>
            </div>
             <input type="hidden" value="<%=classIdList%>" id="oncemore" />
             <input type="hidden" value="<%=orderPrice%>" id="Hidden1" />
             <a href="javascript:void(0);" class="more-p jia" id="dateTips"></a>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    <uc1:nav ID="nav1" runat="server" />
      <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var isajax = true;
            var pageNow = 2;
            var classid = document.getElementById("oncemore").value;
            var type_price = document.getElementById("Hidden1").value;
            jQuery(window).scroll(function () {
                if (($(window).scrollTop()) >= ($(document).height() - $(window).height())) {
                    if (isajax) { jQuery(".jia").click(); }
                }
            });
            jQuery(".jia").click(function () {
                if (isajax) {
                    jQuery.ajax(
                    { type: "POST",
                     url: "productMore.aspx?t=" + Math.random(),
                     data: { "page": pageNow, "classid": classid, "type_price": type_price },
                     dataType: "html",
                     cache: true, 
                     beforeSend: function () {
                        jQuery(".jia").addClass("loadboxing"); 
                        isajax = false;
                    },
                     success: function (data) {
                            if (data != '') {
                                jQuery("#gallery-filtralbe").append(data);
                                isajax = true;
                                pageNow++;
                            }
                            else {
                                isajax = false;
                                $("#dateTips").html("");
                                $("#dateTips").html("暂无更多相关数据");
                            }
                        }, 
                        error: function () { alert("暂无更多相关数据!"); }
                    })
                }
            });
        });
    </script>
    </form>
</body>
</html>
