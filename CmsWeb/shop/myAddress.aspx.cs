﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_myAddress : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Bind_date(user_id);//收货地址赋值
        }
    }

    #region 收货地址赋值=======================
    /// <summary>
    /// 收货地址赋值
    /// </summary>
    /// <param name="user_id"></param>
    public void Bind_date(int user_id)
    {
        DataSet ds = new Cms.BLL.c_user_address().GetList("user_id=" + user_id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepAddress.DataSource = ds.Tables[0].DefaultView;
            RepAddress.DataBind();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                //addressId.Value = ds.Tables[0].Rows[i]["id"].ToString();
            }
        }

    }
    #endregion

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        int id = Convert.ToInt32(((HiddenField)(((RadioButton)sender).Parent.FindControl("Fielddocid"))).Value);
        RadioButton ChB = ((RadioButton)(((RadioButton)sender).Parent.FindControl("RadioButton1")));
        Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_address set is_default=0 where user_id=" + userModel.id);
        if (ChB.Checked == true)
        {
            int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_address set is_default=1  where id=" + id);
        }
        Bind_date(user_id);//收货地址赋值
    }
}