﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;


public partial class shop_exchangeFour : System.Web.UI.Page
{
    protected int user_id;//会员id

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Application["usercard"] = userModel.usercard;

            int order_id = Convert.ToInt32(Request["orderId"].ToString());
            Cms.Model.C_order_integral model = new Cms.BLL.C_order_integral().GetModel(order_id);
            Application["order_num"] = model.order_num;
            if (model.storesId.ToString() != "")
            {
                Cms.Model.sc_stores modelStores = new Cms.BLL.sc_stores().GetModel(Convert.ToInt32(model.storesId));
                storename.Text = modelStores.storename;
                address.Text = modelStores.address;
                tel.Text = modelStores.tel;
            }
            else
            {
                Cms.Model.sc_stores modelStores = new Cms.BLL.sc_stores().GetModel(Convert.ToInt32("1"));
                storename.Text = modelStores.storename;
                address.Text = modelStores.address;
                tel.Text = modelStores.tel;
            }
            Bind_date(order_id);//赋值操作
            GenerationCard(order_id, user_id,Convert.ToInt32(model.storesId));
        }
    }
    #region 赋值操作========================
    public void Bind_date(int order_id)
    {
        DataTable dt = new Cms.BLL.C_order_integral().GetList("id=" + order_id+"").Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        else
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        

    }

    public void RepOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepOrderListSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_order_integralsub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }

    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    #endregion

    #region 生成二维码=============================
    public void GenerationCard(int order_id,int userId,int storesId)
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        Cms.Model.C_order_integral model = new Cms.BLL.C_order_integral().GetModel(order_id);
        WebClient wc = new WebClient();
        String bmpUrl = model.id + ".gif";

        //Response.Write(Server.MapPath("~/Upload/image/" + bmpUrl));
        string picpath = Server.MapPath("~/Upload/image/ewm/" + bmpUrl);

        if (System.IO.File.Exists(Server.MapPath("~/Upload/image/ewm/" + bmpUrl)) == true)
        {
            System.IO.File.Delete(Server.MapPath("~/Upload/image/ewm/" + bmpUrl));
        }
        if (System.IO.Directory.Exists(Server.MapPath("~/Upload/image/ewm/" + bmpUrl)) == false)
        {
            string surl = HttpContext.Current.Request.Url.Host.ToString();
            string link = "http://dbe.zhiqiyun.com/shop/exchangePass.aspx?id=" + order_id + "&userId=" + userModel.id + "&storesId=" + storesId;
            // link = "http://" + surl + "/" + urlname + "?uid=" + userModel.id;
            string RequestUrl = "http://qr.liantu.com/api.php";
            string mecard = "?bg=ffffff&fg=000000&gc=000000&el=l&w=258&m=10&text=" + link;
            RequestUrl += mecard;
            wc.DownloadFile(RequestUrl, Server.MapPath("~/Upload/image/ewm/" + bmpUrl));
            ewm.Src = "/Upload/image/ewm/" + bmpUrl;
        }
        else
        {
            ewm.Src = "/Upload/image/ewm/" + bmpUrl; ;
        }
    }
    #endregion
}