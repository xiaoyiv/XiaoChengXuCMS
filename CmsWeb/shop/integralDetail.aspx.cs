﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_integralDetail : System.Web.UI.Page
{
    protected int user_id;//会员id
    public int userJifen;//会员账户积分
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            this.userJifen = Convert.ToInt32(userModel.userscore);
            int articleId = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//文章ID
            if (new Cms.BLL.C_article().Exists(articleId))
            {
                this.DataBind(articleId);//赋值
                this.addClick(articleId);//增加浏览次数
            }
            else
            {
                JscriptMsg("此商品已下架！", "back", "Error");
            }
        }
    }
    #region 赋值操作=====================
    public void DataBind(int articleId)
    {

        #region 文章表
        Cms.Model.C_article model = new Cms.BLL.C_article().GetModel(articleId);
        Application["title"] = model.title.ToString();
        Application["photoUrl"] = model.photoUrl.ToString();
        Application["content"] = model.content.ToString();
        #endregion

        #region 产品表
        DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + articleId).Tables[0];
        if (dt.Rows.Count > 0)
        {
            Application["userscore"] = Convert.ToInt32(dt.Rows[0]["userscore"]).ToString();
            if (userJifen < Convert.ToInt32(dt.Rows[0]["userscore"]))
            {
                Exchange.InnerHtml = "不可兑";
            }
            else
            {
                Exchange.InnerHtml = "可兑";
            }
            Application["marketPrice"] = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            Application["stock"] = dt.Rows[0]["stock"].ToString();
            Application["integral"] = dt.Rows[0]["integral"].ToString();
            Application["style"] = dt.Rows[0]["style"].ToString();
            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["type"].ToString())))
            {
                Application["type"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["type"].ToString())).title;
            }
            else
            {
                Application["type"] = "";
            }
            Application["kim_joong"] = Convert.ToDecimal(dt.Rows[0]["kim_joong"].ToString() == "" ? "0" : dt.Rows[0]["kim_joong"].ToString()).ToString("0.00");
            Application["inch_number"] = dt.Rows[0]["inch_number"].ToString();
            Application["gold_price"] = Convert.ToDecimal(dt.Rows[0]["gold_price"]).ToString("0.00");
            Application["labor_charges"] = Convert.ToDecimal(dt.Rows[0]["labor_charges"]).ToString("0.00");

            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["color"].ToString() == "" ? "0" : dt.Rows[0]["color"].ToString())))
            {
                Application["color"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["color"].ToString())).title;
            }
            else
            {
                Application["color"] = "";
            }

            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["clarity"].ToString() == "" ? "0" : dt.Rows[0]["clarity"].ToString())))
            {
                Application["clarity"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["clarity"].ToString())).title;
            }
            else
            {
                Application["clarity"] = "";
            }

            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["cut"].ToString() == "" ? "0" : dt.Rows[0]["cut"].ToString())))
            {
                Application["cut"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["cut"].ToString())).title;
            }
            else
            {
                Application["cut"] = "";
            }
            Application["main_stone_weight"] = dt.Rows[0]["main_stone_weight"].ToString();
            Application["main_stone_number"] = dt.Rows[0]["main_stone_number"].ToString();
            Application["total_weight"] = dt.Rows[0]["total_weight"].ToString();
            Application["sales_labor_charge"] = Convert.ToDecimal(dt.Rows[0]["sales_labor_charge"]).ToString("0.00");
            Application["accessories_price"] = Convert.ToDecimal(dt.Rows[0]["accessories_price"]).ToString("0.00");
            Application["product_category"] = dt.Rows[0]["product_category"].ToString();
            Application["auxiliary_stone_weight"] = dt.Rows[0]["auxiliary_stone_weight"].ToString();
            Application["side_stone_number"] = dt.Rows[0]["side_stone_number"].ToString();
            Application["certificate_no"] = dt.Rows[0]["certificate_no"].ToString();

            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["material"].ToString())))
            {
                Application["material"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["material"].ToString())).title;
            }
            else
            {
                Application["material"] = "";
            }
            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["bstype"].ToString())))
            {
                Application["bstype"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["bstype"].ToString())).title;
            }
            else
            {
                Application["bstype"] = "";
            }
            if (new Cms.BLL.C_article_category().Exists(Convert.ToInt32(dt.Rows[0]["pintype"].ToString())))
            {
                Application["pintype"] = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["pintype"].ToString())).title;
            }
            else
            {
                Application["pintype"] = "";
            }

            

            string finger_ring_list = dt.Rows[0]["finger_ring"].ToString();
            string[] sArray = finger_ring_list.Split(',');
            foreach (string i in sArray)
            {
                if (i.ToString() == "")
                {
                    continue;
                }
                DataSet ds_finger_ring = new Cms.BLL.C_article_category().GetList("id=" + Convert.ToInt32(i));
                if (ds_finger_ring != null && ds_finger_ring.Tables[0].Rows.Count > 0)
                {
                    ListItem myitem = new ListItem();
                    myitem.Value = ds_finger_ring.Tables[0].Rows[0]["id"].ToString();
                    myitem.Text = ds_finger_ring.Tables[0].Rows[0]["title"].ToString();
                    finger_ring.Items.Add(myitem);
                }
            }
        }
        #endregion

        #region 相册表
        DataSet dsPhoto = new Cms.BLL.C_article_albums().GetList("article_id=" + articleId);
        if (dsPhoto != null && dsPhoto.Tables[0].Rows.Count > 0)
        {
            RepeaterPhoto.DataSource = dsPhoto.Tables[0].DefaultView;
            RepeaterPhoto.DataBind();
        }
        #endregion

        #region 购买记录
        DataSet ds = new Cms.BLL.C_ordersub().GetList("article_id=" + articleId);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepBuyRecord.DataSource = ds.Tables[0].DefaultView;
            RepBuyRecord.DataBind();
        }
        #endregion
    }
    #endregion

    #region 增加浏览次数============================
    public void addClick(int articleId)
    {
        int counts = Cms.DBUtility.DbHelperSQL.ExecuteSql("update C_article set hits=hits+1 where articleId=" + articleId + "");
    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion

    #region 兑换商品==========================
    protected void Button1_Click(object sender, EventArgs e)
    {
       
        Cms.BLL.C_exchange_cart bll = new Cms.BLL.C_exchange_cart();
        Cms.Model.C_exchange_cart model = new Cms.Model.C_exchange_cart();

        #region 兑换购物车======
        int articleId = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//文章ID
        model.article_id = articleId;
        model.title = new Cms.BLL.C_article().GetModel(articleId).title.ToString();
        DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + articleId).Tables[0];
        if (dt.Rows.Count > 0)
        {
            model.price = Convert.ToDecimal(dt.Rows[0]["marketPrice"]);
            model.integral = Convert.ToInt32(dt.Rows[0]["userscore"]);
        }
        model.quantity = Convert.ToInt32(strQuantity.Value);
        Cms.Model.C_user user_model = adminUser.GetuserLoginState();//验证用户是否登录
        model.user_id = user_model.id;
        model.is_checked = 1;
        model.property_value = finger_ring.SelectedValue;
        model.note = note.Value;
        model.updateTime = DateTime.Now;
        int result = 0;
        if (user_model.userscore < Convert.ToInt32(dt.Rows[0]["userscore"]))
        {
            JscriptMsg("积分不足，请兑换其他商品！", "NotJump", "Error");

        }
        else
        {
            if (Cms.DBUtility.DbHelperSQL.Exists("select count(1) from C_exchange_cart where article_id=" + articleId + "and user_id=" + user_model.id))
            {
                result = Cms.DBUtility.DbHelperSQL.ExecuteSql("update C_exchange_cart set quantity=" + Convert.ToInt32(strQuantity.Value) + ",updateTime='" + DateTime.Now + "',integral=" + model.integral + ",price=" + model.price + " where article_id=" + articleId + " and user_id=" + user_model.id);

            }
            else
            {
                result = bll.Add(model);
            }
            if (result > 0)
            {
                JscriptMsg("兑换成功！", "/shop/exchangeOne.aspx", "Success");
            }
            else
            {
                JscriptMsg("兑换失败！", "NotJump", "Error");
            }
        }

        #endregion
    }
    #endregion

    #region 收藏=====================================
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Cms.BLL.C_collect bll = new Cms.BLL.C_collect();
        Cms.Model.C_collect model = new Cms.Model.C_collect();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;

        int articleId = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//文章ID
        DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + articleId).Tables[0];
        if (dt.Rows.Count > 0)
        {
            model.article_id = articleId;
            model.user_id = user_id;
            model.typeId = Convert.ToInt32("2");//收藏分类id
            model.title = new Cms.BLL.C_article().GetModel(articleId).title.ToString();
            model.price = Convert.ToDecimal(dt.Rows[0]["price"]);
            model.integral = Convert.ToInt32(dt.Rows[0]["userscore"]);
            model.updateTime = DateTime.Now;
            if (Cms.DBUtility.DbHelperSQL.Exists("select count(1) from C_collect where article_id=" + articleId + "and user_id=" + user_id))
            {
                JscriptMsg("您已收藏过了！", "NotJump", "Error");
            }
            else
            {
                int result = bll.Add(model);
                if (result > 0)
                {
                    JscriptMsg("收藏成功！", "NotJump", "Success");
                }
                else
                {
                    JscriptMsg("收藏失败！", "NotJump", "Error");
                }
            }

        }
    }
    #endregion
}