﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_integralMore : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
    public void Bind()
    {
        int pagenow = 0;
        string classid = "";
        if (Request.Params["page"] != null)
        {
            pagenow = Convert.ToInt32(Request.Params["page"]);
        }
        if (Request.Params["classid"] != null)
        {
            classid = Request.Params["classid"];
        }
        DataSet ds = new Cms.BLL.C_article().GetList("parentId in (" + classid + ")");
        int count = ds.Tables[0].Rows.Count;
        //if (count % 4 == 1)
        //{
        int start = (pagenow - 1) * 4 + 1;
        int end = pagenow * 4;

        //string tearname = new Cms.BLL.C_Column().GetModel(pid).name.ToString();
        DataSet ds_teacher = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from C_article T WHERE parentId in (" + classid + ")  ) TT WHERE TT.Row between " + start + " and " + end);
        //ds = bll.GetListByPage("parentId=" + pid, "updatetime desc", start, end);
        if (ds_teacher != null && ds_teacher.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        else
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        //Response.Write(count%4);
        //}
    }
    /// <summary>
    /// 获取商品积分
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getIntegral(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["userscore"].ToString();
            }
        }
        return result;
    }
    /// <summary>
    /// 获取商品市场价格
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getMprice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            }
        }
        return result;
    }
}