﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shop_ashx_Tools : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"].ToString();
        switch (action)
        {
            case "sendSms":
                sendSms();
                break;
        }
    }
    public class SmsApi
    {

        public string rescode { get; set; }

        public string message { get; set; }

    }
    public void sendSms()
    {
        string result = "";
        string code = Cms.Common.Utils.Number(6);
        //context.Session["Verification_code"] = ""; //随机生成6位数字
        string telphone = Request.QueryString["telphone"].ToString();
        string content = "您当前的手机验证码：" + code;

        Cms.BLL.C_sms bll = new Cms.BLL.C_sms();
        Cms.Model.C_sms model = new Cms.Model.C_sms();
        model.name = "";//名字
        model.telphone = telphone;//手机号码
        model.content = content;//内容
        model.state = 0;//发送状态
        model.updateTime = Convert.ToDateTime(Cms.Common.ManagementInfo.GetTime());//时间
        int count = bll.Add(model);
        if (count > 0)
        {
            string smsResult = adminUser.Sms(telphone, content);
            SmsApi p = Cms.Common.Utils.JsonDeserialize<SmsApi>(smsResult);
            if (p.rescode == "0")
            {
                int counts = Cms.DBUtility.DbHelperSQL.ExecuteSql("update C_sms set state=1 where id=" + result);//修改状态
                result = code;
            }
            else
            {
                result = "0";
            }

        }
        else
        {
            result = "0";

        }
        Response.Write(result);
        return;
    }
}