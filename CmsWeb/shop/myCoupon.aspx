﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myCoupon.aspx.cs" Inherits="shop_myCoupon" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">我的优惠券</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="coupon">
                <ul class="coupon-ul">
                <asp:Repeater runat="server" ID="RepCoupon">
                <ItemTemplate>
                    <li>
                    	<div class="coupon-l">
                        	<img src="<%#Eval("picurl")%>">
                            <div class="coupon-r">
                            	<h3><%#Eval("title")%></h3>
                                <p>有效期：<%#Convert.ToDateTime(Eval("stime")).ToString("yyyy/MM/dd")%>-<%#Convert.ToDateTime(Eval("etime")).ToString("yyyy/MM/dd")%></p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <a href="javascript:void(0)"><%#Eval("status").ToString()=="0"?"未使用":"已使用"%></a>
                        <div class="clear"></div>
                    </li>
                 </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
