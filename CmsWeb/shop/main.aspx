﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="shop_main" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="/shop/main.aspx" class="page-title">用户中心</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="user-admin">
            	<div class="user-admin-info" style="display:none;">
                	<a href="javascript:void(0)" class="user-admin-img"><img src="images/user_pic1.jpg"></a>
                    <div class="user-new">
                        <a href="#" class="user-old">已是会员？绑定会员卡！</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <!--登录后信息-->
                <div class="user-admin-info user-admin-info-ed" >
                	<a href="#" class="user-admin-img"><img src="<%=Application["headimgurl"].ToString()==""? "images/user_pic1.jpg" : Application["headimgurl"].ToString()%>"></a>
                    <div class="user-new">
                    	<h3>Hi，<%=Application["username"] %></h3>
                        <%--<p>余额：￥<%=user_money.ToString("0.00")%>元<span><br /></span>--%>积分：<%=userJifen.ToString()%>积分</p>
                     <a href="/shop/bindUser.aspx" class="user-old" id="bindUser" runat="server">绑定实体店会员卡！</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <!--登录后信息-->
              <%--  <ul class="user-status">
                	<li class="user-status1"><a href="/shop/myOrder.aspx"><span class="user-value"><%=Application["payCount"]%></span><p>待付款</p></a></li>
                    <li class="user-status2"><a href="/shop/myOrder.aspx"><span><%=Application["is_delivery"]%></span><p>待发货</p></a></li>
                    <li><a href="/shop/myOrder.aspx"><span class="user-value"><%=Application["is_receiving"] %></span><p>待收货</p></a></li>
                    <li><a href="/shop/myOrder.aspx"><span>0</span><p>待评价</p></a></li>
                    <li><a href="/shop/myOrder.aspx"><span>0</span><p>售后</p></a></li>
                    <div class="clear"></div>
                </ul>--%>
            </div>
            <div class="user-admin-list">
            	<ul class="user-ul">
                	<li class="user-ul-li1"><a href="/shop/myOrder.aspx">全部订单<i></i></a></li>
                    <li class="user-ul-li2"><a href="/shop/user.aspx">个人信息<i></i></a></li>
                    <li class="user-ul-li11"><a href="/shop/exchangeThree.aspx">兑换订单<i></i></a></li>
                </ul>
                <div class="user-ul-1 mb10">
                	<ul class="user-ul-11">
                    	<li class="user-ul-li3"><a href="/shop/myCoupon.aspx">我的优惠券<i></i></a></li>
                   		<li class="user-ul-li4"><a href="/shop/myIntegral.aspx">我的积分<i></i></a></li>
                        <%--<li class="user-ul-li5"><a href="/shop/newCard.aspx">开通实体店会员<i></i></a></li>--%>
                   		<li class="user-ul-li6"><a href="/shop/myCard.aspx">我的会员卡<i></i></a></li>
                    </ul>
                </div>
                <div class="user-ul-1">
                	<ul class="user-ul-11">
                    	<li class="user-ul-li7"><a href="/shop/myCollect.aspx">我的收藏<i></i></a></li>
                   		<li class="user-ul-li8"><a href="/shop/myAddress.aspx">地址管理<i></i></a></li>
                        <li class="user-ul-li9"><a href="/shop/Integral.aspx">积分兑换<i></i></a></li>
                   		<li class="user-ul-li10"><a href="/shop/cart.aspx">我的购物车<i></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="product-button1">
<asp:LinkButton ID="LinkButton1" CssClass="login_ipt bgc8c8c8" runat="server" 
                    onclick="LinkButton1_Click">退出登录</asp:LinkButton>
                
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>

    </form>
</body>
</html>
