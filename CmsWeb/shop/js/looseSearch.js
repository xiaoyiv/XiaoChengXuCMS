﻿function dpshare() {
    document.getElementById("share").style.display = "block";
}

function changeHidden(name) {
    var sibarr = $('#' + name + "_list>div");
    var value = "";
    for (i = 0; i < sibarr.length; i++) {
        if (name == 'Shape') {
            if ($(sibarr[i]).find('img').attr("src").search(/_on.jpg/) != -1) {
                value += $(sibarr[i]).find('img').attr("ref") + ",";
            }
        } else {
            if ($(sibarr[i]).find("div").attr("class").indexOf('on') != -1) {
                value += $(sibarr[i]).find("div").attr("ref") + ",";
            }
        }
    }
    if (value != "") {
        $("#" + name).val(value);
    } else {
        $("#" + name).val(",");
    }
}

$(function () {
    $('.row>div').click(function () {
        if ($(this).find('div').hasClass("on")) {
            $(this).find('div').removeClass("on");
        } else {
            $(this).find('div').addClass("on");
        }
        changeHidden($(this).parent().attr('ref'));
    })

    $('#search_btn').click(function () {
        $("#diamond_search_form").submit();
    });

    $("#reset_btn").click(function () {
        location.reload();
    });

    $(".shape_box img").each(function (index, element) {
        $(element).click(function () {
            if ($(this).attr("src").search(/_on.jpg/) == -1) {
                var ns = $(this).attr("src").replace(/.jpg/, "_on.jpg");
                $(this).attr("src", ns);
                $(this).css("border", "1px solid #aeaeae");
            } else {
                var as = $(this).attr("src").replace(/_on.jpg/, ".jpg");
                $(this).attr("src", as);
                $(this).css("border", "1px solid #fafafa");
            }
            changeHidden('Shape');
        })
    })
});

// var amts=[0,10000,30000,130000,330000,1330000,3330000,13330000];

//     function changeSldC(){
//         var begNum = parseInt( $(".carat_beg").val());
//         var endNum = parseInt( $(".carat_end").val());
//         $("#sliderCarat").slider( "option", "values", [begNum,endNum] );
//     }

//     function changeSldP(){
//         var begNum = parseInt( $(".price_beg").val());
//         var endNum = parseInt( $(".price_end").val());
//         $("#sliderPrice").slider( "option", "values", [begNum,endNum] );
//     }


//   $(function() {
//     $( "#sliderCarat" ).slider({
//         min: 0,
//         max: 20,
//         step: 0.1,
//       range: true,
//       values: [ 0, 20],
//             slide: function(event,ui){
//                 $( ".carat_beg" ).val(ui.values[0]);
//                 $( ".carat_end" ).val(ui.values[1]);
//             }
//       });
//         $(".carat_beg").val( $( "#sliderCarat" ).slider( "values", 0 ));
//         $(".carat_end").val( $( "#sliderCarat" ).slider( "values", 1 ));


//         $( "#sliderPrice" ).slider({
//             min: 0,
//             max: 5000000,
//             step: 5000,
//         range: true,
//         values: [ 0, 5000000],
//         slide: function(event,ui){
//           $(".price_beg").val(ui.values[0]);
//           $(".price_end").val(ui.values[1]);
//       }
//     });
//         $(".price_beg").val( $( "#sliderPrice" ).slider( "values", 0 ));
//         $(".price_end").val( $( "#sliderPrice" ).slider( "values", 1 ));

// });

$(document).ready(function () {

    var Wprice = 0;
    var Wcarat = 0;

    $("#range_price").ionRangeSlider({
        min: 0,
        max: 1000000,
        from: 0,
        to: 1000000,
        type: 'double',
        step: 5000,
        prefix: "¥",
        maxPostfix: "+",
        prettify: true,
        hasGrid: false,
        gridMargin: 8,
        onLoad: function (obj) {
            Wprice = obj.max;
        },
        onFinish: function (obj) {
            $("#J_1").val(obj.fromNumber);
            $("#J_2").val(obj.toNumber);
        }

    });


    $("#range_carat").ionRangeSlider({
        min: 0,
        max: 3,
        from: 0,
        to: 3,
        type: 'double',
        step: 0.1,
        postfix: " 克拉",
        prettify: true,
        hasGrid: true,
        gridMargin: 8,
        onLoad: function (obj) {
            Wcarat = obj.max;
        },
        onFinish: function (obj) {
            $("#S_1").val(obj.fromNumber);
            $("#S_2").val(obj.toNumber);
        }

    });



    $("#Cprice").bind({ "touchend": function () {

        //alert(Wprice);
        if (Wprice == 1000000) {
            Wprice = 150000;
            $("#range_price").ionRangeSlider("update", {
                min: 0,                        // change min value
                max: 150000,                        // change max value
                from: 0,                       // change default FROM setting
                to: 150000,                         // change default TO setting
                step: 500,
                maxPostfix: ""                // change slider step
            }
            );
            $("#Cprice").text("扩大价格选择范围");
        } else {
            Wprice = 1000000;
            $("#range_price").ionRangeSlider("update", {
                min: 0,                        // change min value
                max: 1000000,                        // change max value
                from: 0,                       // change default FROM setting
                to: 1000000,                         // change default TO setting
                step: 5000,
                maxPostfix: "+"                         // change slider step
            }
            );
            $("#Cprice").text("缩小价格选择范围");
        }
        return false;
    },
        "click": function () {

            //alert(Wprice);
            if (Wprice == 1000000) {
                Wprice = 150000;
                $("#range_price").ionRangeSlider("update", {
                    min: 0,                        // change min value
                    max: 150000,                        // change max value
                    from: 0,                       // change default FROM setting
                    to: 150000,                         // change default TO setting
                    step: 500,
                    maxPostfix: ""                // change slider step
                }
            );
                $("#Cprice").text("扩大价格选择范围");
            } else {
                Wprice = 1000000;
                $("#range_price").ionRangeSlider("update", {
                    min: 0,                        // change min value
                    max: 1000000,                        // change max value
                    from: 0,                       // change default FROM setting
                    to: 1000000,                         // change default TO setting
                    step: 5000,
                    maxPostfix: "+"                         // change slider step
                }
            );
                $("#Cprice").text("缩小价格选择范围");
            }
            return false;
        }
    })

    $("#Ccarat").bind({ "touchend": function () {
        //alert(Wcarat);
        if (Wcarat == 3) {
            Wcarat = 20;
            $("#range_carat").ionRangeSlider("update", {
                min: 0,                        // change min value
                max: 20,                        // change max value
                from: 0,                       // change default FROM setting
                to: 20,                         // change default TO setting
                step: .1                // change slider step
            }
            );
            $("#Ccarat").text("缩小钻重选择范围");
        } else {
            Wcarat = 3;
            $("#range_carat").ionRangeSlider("update", {
                min: 0,                        // change min value
                max: 3,                        // change max value
                from: 0,                       // change default FROM setting
                to: 3,                         // change default TO setting
                step: .1                         // change slider step
            }
            );
            $("#Ccarat").text("扩大钻重选择范围");
        }
        return false;
    },
        "click": function () {
            //alert(Wcarat);
            if (Wcarat == 3) {
                Wcarat = 20;
                $("#range_carat").ionRangeSlider("update", {
                    min: 0,                        // change min value
                    max: 20,                        // change max value
                    from: 0,                       // change default FROM setting
                    to: 20,                         // change default TO setting
                    step: .1                // change slider step
                }
            );
                $("#Ccarat").text("缩小钻重选择范围");
            } else {
                Wcarat = 3;
                $("#range_carat").ionRangeSlider("update", {
                    min: 0,                        // change min value
                    max: 3,                        // change max value
                    from: 0,                       // change default FROM setting
                    to: 3,                         // change default TO setting
                    step: .1                         // change slider step
                }
            );
                $("#Ccarat").text("扩大钻重选择范围");
            }
            return false;
        }
    })



});

//绑定需要浮动的表头
$(function () {
    $(".rule-multi-checkbox").ruleMultiCheckbox();
    $(".rule-multi-checkboxcolor").ruleMultiCheckboxcolor();

});
//多项复选框
$.fn.ruleMultiCheckbox = function () {
    var multiCheckbox = function (parentObj) {
        parentObj.addClass("multi-checkbox"); //添加样式
        parentObj.children().hide(); //隐藏内容
        var divObj = $('<div class="boxwrap"></div>').prependTo(parentObj); //前插入一个DIV
        parentObj.find(":checkbox").each(function () {
            var indexNum = parentObj.find(":checkbox").index(this); //当前索引
            var newObj = $('<a href="javascript:;"><img src="' + parentObj.find('label').eq(indexNum).parent().attr("src") + '" /><br/>' + parentObj.find('label').eq(indexNum).text() + '</a>').appendTo(divObj); //查找对应Label创建选项
            if ($(this).prop("checked") == true) {
                newObj.addClass("selected"); //默认选中
            }
            //检查控件是否启用
            if ($(this).prop("disabled") == true) {
                newObj.css("cursor", "default");
                return;
            }
            //绑定事件
            $(newObj).click(function () {
                if ($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                    //parentObj.find(':checkbox').eq(indexNum).prop("checked",false);
                } else {
                    $(this).addClass("selected");
                    //parentObj.find(':checkbox').eq(indexNum).prop("checked",true);
                }
                parentObj.find(':checkbox').eq(indexNum).trigger("click"); //触发对应的checkbox的click事件
                //alert(parentObj.find(':checkbox').eq(indexNum).prop("checked"));
            });
        });
    };
    return $(this).each(function () {
        multiCheckbox($(this));
    });
}

//多项复选框
$.fn.ruleMultiCheckboxcolor = function () {
    var multiCheckbox = function (parentObj) {
        parentObj.addClass("multi-checkboxcolor"); //添加样式
        parentObj.children().hide(); //隐藏内容
        var divObj = $('<div class="boxwrap"></div>').prependTo(parentObj); //前插入一个DIV
        parentObj.find(":checkbox").each(function () {
            var indexNum = parentObj.find(":checkbox").index(this); //当前索引
            var newObj = $('<a href="javascript:;">' + parentObj.find('label').eq(indexNum).text() + '</a>').appendTo(divObj); //查找对应Label创建选项
            if ($(this).prop("checked") == true) {
                newObj.addClass("selected"); //默认选中
            }
            //检查控件是否启用
            if ($(this).prop("disabled") == true) {
                newObj.css("cursor", "default");
                return;
            }
            //绑定事件
            $(newObj).click(function () {
                if ($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                    //parentObj.find(':checkbox').eq(indexNum).prop("checked",false);
                } else {
                    $(this).addClass("selected");
                    //parentObj.find(':checkbox').eq(indexNum).prop("checked",true);
                }
                parentObj.find(':checkbox').eq(indexNum).trigger("click"); //触发对应的checkbox的click事件
                //alert(parentObj.find(':checkbox').eq(indexNum).prop("checked"));
            });
        });
    };
    return $(this).each(function () {
        multiCheckbox($(this));
    });
}