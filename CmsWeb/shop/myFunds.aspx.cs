﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_myFunds : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Bind_date();

        }
    }

    #region 赋值操作========================
    public void Bind_date()
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        Application["username"] = userModel.username;
        Application["usercard"] = userModel.usercard;
        Application["userYesScore"] = userModel.userYesScore;
        Application["sex"] = userModel.sex == "M" ? "男" : "女";
        Application["birthday"] = string.Format("{0:yyyy-MM-dd}", userModel.birthday);
        Application["marryday"] = string.Format("{0:yyyy-MM-dd}", userModel.marryday);
        Application["userscore"] = userModel.userscore;
        Application["userMoney"] = Convert.ToDecimal(userModel.userMoney).ToString("0.00");
        Application["allbuy"] = Convert.ToDecimal(userModel.allbuy).ToString("0.00");
        Application["userallscore"] = userModel.userallscore;
    }
    #endregion
}