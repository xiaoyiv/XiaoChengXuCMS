﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using Cms.Common;

public partial class shop_cart : System.Web.UI.Page
{
    protected int user_id;//会员id
    public Double dTotal = 0.00;//总计款额

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set is_checked=1  where user_id=" + user_id);//设置购物车的默认选中状态1
            Bind_date(user_id);//数据赋值
        }
    }

    #region 购物车赋值操作==================================
    /// <summary>
    /// 购物车赋值
    /// </summary>
    public void Bind_date(int user_id)
    {

        DataSet ds = new Cms.BLL.C_user_cart().GetList("user_id=" + user_id);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = ds.Tables[0].DefaultView;
            RepList.DataBind();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["is_checked"].ToString() == "2")
                {
                    dTotal += Convert.ToDouble(ds.Tables[0].Rows[i]["price"]) * Convert.ToInt32(ds.Tables[0].Rows[i]["quantity"]);
                }
            }
        }
        else
        {
            RepList.DataSource = ds.Tables[0].DefaultView;
            RepList.DataBind();
        }
    }
    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }
    /// <summary>
    /// 加减数量
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void RepList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int id = Convert.ToInt32(((HiddenField)e.Item.FindControl("Fielddocid")).Value);
        switch (e.CommandName)
        {

            case "lbtnAdd":
                update(id, "+");
                break;
            case "lbtnLess":
                update(id, "-");
                break;
        }
    }


    protected void RepList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


    }
    /// <summary>
    /// 加减数量
    /// </summary>
    /// <param name="id"></param>
    public void update(int id, string action)
    {

        if (new Cms.BLL.C_user_cart().GetModel(id).quantity != 1 || action != "-")
        {
            int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set quantity=quantity" + action + "1 where id=" + id);
        }
        //重新绑定
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        Bind_date(user_id);//数据赋值
    }


    #endregion

    #region 单选框改变事件====================
    /// <summary>
    /// 单选框改变事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Check_Select_CheckedChanged(object sender, EventArgs e)
    {
        //Response.Write("<script>alert('" + 0 + "')</script>");
        foreach (RepeaterItem rt in this.RepList.Items)
        {
            int id = Convert.ToInt32(((HiddenField)(((CheckBox)sender).Parent.FindControl("Fielddocid"))).Value);
            CheckBox ChB = ((CheckBox)(((CheckBox)sender).Parent.FindControl("Check_Select")));
            if (ChB.Checked == true)
            {
                int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set is_checked=2  where id=" + id);
            }
            else
            {
                int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set is_checked=1  where id=" + id);
            }
        }
        //重新绑定
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        Bind_date(user_id);//数据赋值
    }
    #endregion

    #region 全选单选框===========================
    /// <summary>
    /// 全选单选框
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        string checkBox = checkBoxAll.InnerHtml.ToString();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        if (checkBox == "全选")
        {
            int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set is_checked=2  where user_id=" + user_id);
            checkBoxAll.InnerHtml = "取消";
        }
        else
        {
            int count = Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_cart set is_checked=1  where user_id=" + user_id);
            checkBoxAll.InnerHtml = "全选";
        }
        dTotal = 0.00;
        Bind_date(user_id);//数据赋值

    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion

    #region 结算=================================
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        if (Cms.DBUtility.DbHelperSQL.Exists("select count(1) from c_user_cart where is_checked=2 and user_id=" + user_id))
        {
            Response.Redirect("/shop/Clearing.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "JsPrint", "parent.jsprintWeb(\"请选择需要结算的商品！\", \"NotJump\", \"Error\")", true);
        }
    }
    #endregion

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        foreach (RepeaterItem item in RepList.Items)
        {
            Cms.BLL.C_user_cart bll = new Cms.BLL.C_user_cart();
            //获取选择框
            CheckBox check = item.FindControl("Check_Select") as CheckBox;
            if (check.Checked)
            {
                HiddenField field = item.FindControl("Fielddocid") as HiddenField;
                int id = int.Parse(field.Value);
                //删除文档的同时删除静态文档
                bll.Delete(id);
                // bool bl=adminUser.AddUserLog(DTEnums.ActionEnum.Delete.ToString(), "删除购物车：" + bll.GetModel(id).title); //记录日志
            }
        }
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        Bind_date(user_id);//数据赋值 
        ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "JsPrint", "parent.jsprintWeb(\"删除成功！\", \"/shop/cart.aspx\", \"Success\")", true);
        //JscriptMsg("删除信息成功！", "NotJump", "Success");
    }
}