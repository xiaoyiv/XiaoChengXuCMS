﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="user.aspx.cs" Inherits="shop_user" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="/shop/user.aspx" class="page-title">个人信息</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="info-t">
                <dl>
                    <dt>会员姓名：</dt>
                    <dd><%=Application["username"] %></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>会员卡号：</dt>
                    <dd><%=Application["usercard"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>会员级别：</dt>
                    <dd><%=Application["userlevel"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>会员性别：</dt>
                    <dd><%=Application["sex"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>会员生日：</dt>
                    <dd><%=Application["birthday"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>结婚日期：</dt>
                    <dd><%=Application["marryday"]%></dd>
                    <div class="clear"></div>
                </dl>
            </div>
            <div class="info-t">
                <dl>
                    <dt>您的电话：</dt>
                    <dd><%=Application["telphone"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>居住地址：</dt>
                    <dd><%=Application["useraddress"]%></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>所属门店：</dt>
                    <dd><%=Application["shopname"]%></dd>
                    <div class="clear"></div>
                </dl>
            </div>
        	<div class="product-button1 product-button2">
          <a href="/shop/userEdit.aspx" class="login_ipt">修改个人信息</a>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
