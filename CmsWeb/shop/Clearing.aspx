﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clearing.aspx.cs" Inherits="shop_Clearing" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/customInput.jquery.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="javascript:void(0);" class="page-title">订单信息</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="address">
                <a href="/shop/EditAddress.aspx?action=add" class="add_adre" style="display:none;">添加收货地址</a>
                <div class="addre_c">
                    <div class="addre_cc">
                    <asp:Repeater runat="server" ID="RepAddress">
                    <ItemTemplate>
                    	<a href="/shop/EditAddress.aspx?action=edit&id=<%#Eval("id")%>&ReturnPage=/shop/Clearing.aspx">
                            <h3>收货人：<%#Eval("consignee")%><span><%#Eval("cellphone")%></span></h3>
                            <p>收货地址：<%#Eval("address")%></p>
                        </a>
                    </ItemTemplate>
                    </asp:Repeater>
             <asp:HiddenField ID="addressId" runat="server"></asp:HiddenField>
               <asp:HiddenField ID="price_sum" runat="server"></asp:HiddenField>
                 <asp:HiddenField ID="quantity_sum" runat="server"></asp:HiddenField>
                 <asp:HiddenField ID="integral_sum" runat="server"></asp:HiddenField>
                    </div>
                </div>
            </div>
            <div class="bill">
            	<div class="bill-title">
                	<h2>购物清单</h2>
                    <p>共<%=QTotal.ToString()%>件<span>¥ <%=dTotal.ToString("0.00")%></span></p>
                    <div class="clear"></div>
                </div>
            	<ul class="bill_ul">
                    <li>
                <asp:Repeater runat="server" ID="RepList" OnItemCommand="RepList_ItemCommand">
                <ItemTemplate>
                        <div class="bill_li_1">
                            <a href="#">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>">
                                <p><%#Eval("title")%><span>价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%><i>数量：<%#Eval("quantity") %></i></span></p>
                                <div class="clear"></div>
                            </a>
                        </div>
                 </ItemTemplate>
                  <FooterTemplate>
                <%#RepList.Items.Count == 0 ? "<div style='text-align:center;'>暂无购物记录</div>" : ""%>
              
            </FooterTemplate>
                </asp:Repeater>
                    </li>
                </ul>
                <div class="bill-tic">
                	<p><a href="javascript:void(0);"><span>优惠券：</span><asp:DropDownList 
                            ID="DropDownList1"  runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList></a></p>
                    <p><a href="javascript:void(0);"><span>现金券：</span><asp:DropDownList 
                            ID="DropDownList2" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList></a></p>
                    <%--<p><a href="javascript:void(0);"><span>账户余额：</span><%=user_money.ToString("0.00")%>元</a></p>--%>
                </div>
                <div class="bill-mes">
                	<input name="recommended_code" id="recommended_code" runat="server" value="" type="text" class="ma-input" placeholder="推荐码："/>
                </div>
                <div class="bill-ma">
                    <div class="bill-mes">
                        <input name="note" ID="note" runat="server" value="" type="text" class="ma-input" placeholder="给卖家留言："/>
                    </div>
                </div>
            </div>
            <div class="bill-pay">
                <div class="bill-title">
                    <h2>支付方式</h2>
                    <div class="clear"></div>
                </div>
                <ul class="bill-pay-ul">
                    <li><img src="images/wei.png">

                        <asp:RadioButton ID="RadioButton1" Name="RadioButton1" Checked="true" GroupName="pay" Text="微支付" runat="server"> </asp:RadioButton>
                        <div class="clear"></div>
                    </li>
                    <%--<li>
                    <img src="images/zhi.png">
                        
                       <asp:RadioButton ID="RadioButton2" Name="RadioButton2" GroupName="pay" Text="支付宝" runat="server"> </asp:RadioButton>
                        <div class="clear"></div>
                    </li>--%>
                </ul>
                 <%--<a href="#" class="pay-more"><span>更多支付方式</span></a>--%>
            </div>
            <div class="bill-send">
                <div class="bill-title">
                    <h2>配送方式</h2>
                    <div class="clear"></div>
                </div>
                <ul class="bill-send-ul">
                    <li>
                        <asp:RadioButton ID="RadioButton3" Name="RadioButton3" GroupName="shopping" Text="门店提货方式" runat="server"></asp:RadioButton>
                        <div class="clear"></div>
                    </li>
                    <li>
                     <asp:RadioButton ID="RadioButton4" Name="RadioButton4" GroupName="shopping" Checked="true" Text="快递免邮" runat="server"></asp:RadioButton>
                       
                       
                        <div class="clear"></div>
                    </li>
                </ul>
            </div>
            <%-- <div class="dbe-borbg use-integ">
              <asp:RadioButton ID="RadioButton5" GroupName="integl" Text="使用积分" runat="server"></asp:RadioButton>
                  <input name="" type="text" id="integralArrived" runat="server" value="10000" class="integ-input"/>点
                        <b>- ¥100.00</b>
                    <!--选择使用积分后显示-->
                  
                    <!--选择使用积分后显示-->
                    <div class="clear"></div>
               
            </div>--%>
            <div class="bill-total">
                <p>共<%=QTotal.ToString()%>件，<b>¥ <%=dTotal.ToString("0.00")%></b></p>
                <span>可获得积分：<%=dintegral%>点</span>
    
            </div>
            <div class="product-button1">
<asp:Button ID="Button1" runat="server" CssClass="login_ipt" Text="提交订单" 
                    onclick="Button1_Click"></asp:Button>
            </div>
        </div>
    </section>
         <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
</section>
    <script type="text/javascript">        $('input').customInput();</script>
    </form>
</body>
</html>
