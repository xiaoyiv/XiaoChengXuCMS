﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bigWheel.aspx.cs" Inherits="shop_bigWheel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
      <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .demo
        {
            width: 417px;
            height: 417px;
            position: relative;
            margin: 50px auto;
        }
        #disk
        {
            width: 417px;
            height: 417px;
            background: url(images/disk.jpg) no-repeat;
        }
        #start
        {
            width: 163px;
            height: 320px;
            position: absolute;
            top: 46px;
            left: 130px;
        }
        #start img
        {
            cursor: pointer;
        }
    </style>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jQueryRotate.2.2.js"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#startbtn").click(function () {
                lottery();
            });
        });

        function lottery() {
            $.ajax({
                type: 'POST',
                url: 'go.aspx',
                dataType: 'json',
                cache: false,
                error: function (data) {
                    alert("出现错误");
                    return false;
                },
                success: function (json) {
                    $("#startbtn").unbind('click').css("cursor", "default");
                    var a = json.angle; //角度 
                    var p = json.prize; //奖项 


                    $("#startbtn").rotate({
                        duration: 3000, //转动时间 
                        angle: 0,
                        animateTo: 1800 + a, //转动角度 
                        easing: $.easing.easeOutSine,
                        callback: function () {
                            var con = confirm('恭喜你，中得' + p + '\n还要再来一次吗？');
                            if (con) {
                                lottery();
                            } else {
                                return false;
                            }
                        }
                    });

                }
            });
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0)" class="page-title">幸运大转盘</a>
    </header>
    <section class="main main-product">
       <div id="main">
   <div class="msg"></div>
   <div class="demo">
        <div id="disk"></div>
        <div id="start"><img src="images/start.png" id="startbtn"></div>
   </div>
        </div>
    </section>
</section>
    </form>
</body>
</html>
