﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_myIntegral : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            bind_user(user_id);//赋值操作

            bind_integral(user_id);//赋值积分记录
        }
    }

    #region 赋值会员信息=============
    public void bind_user(int user_id)
    {
        Cms.Model.C_user userModel = new Cms.BLL.C_user().GetModel(user_id);
        Application["usercard"] = userModel.usercard;
        Application["userscore"] = userModel.userscore;
        Application["userYesScore"] = userModel.userYesScore;
    }
    #endregion

    #region 赋值积分记录=============
    public void bind_integral(int user_id)
    {
        DataTable dt = new Cms.BLL.C_integral_rec().GetList("user_id=" + user_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepIntegral.DataSource = dt;
            RepIntegral.DataBind();
        }
    }
    /// <summary>
    /// 获取封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        else
        {
            result = "images/integ_pic1.jpg";
        }
        return result;
    }
    #endregion
}