﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="副本 myCollect.aspx.cs" Inherits="shop_myCollect" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE戴维克珠宝首饰</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">我的收藏</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
            <div class="mine-order gift-list mycollect">
                <ul>
                <asp:Repeater runat="server" ID="RepOrderList">
                <ItemTemplate>
                    <li>
                        <h2><span>卡号：<%=Application["usercard"]%></span>收藏时间：<%#Eval("updateTime")%></h2>
                        <div class="m-order-goods">
                            <a href="<%#getUrl(Eval("id").ToString())%>">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                <div class="m-order-name">
                                    <h3><%#Eval("title")%></h3>
                                    <p><span> <%#Eval("integral").ToString() == "0" ? "" : "<b>积分兑换：</b>" + Eval("integral") + "积分"%>
                                    <%#Convert.ToDecimal(Eval("price")).ToString("0.00") == "0.00" ? "" : " <b> 价格：¥ </b>" + Convert.ToDecimal(Eval("price")).ToString("0.00")%> </span></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                        </div>
                    </li>
                </ItemTemplate>
                  <FooterTemplate>
                            <%#RepOrderList.Items.Count == 0 ? "<div style='text-align:center;'>暂无收藏记录</div>" : ""%>
                  </FooterTemplate>
                </asp:Repeater>
                </ul>
            </div>
        </div>
             <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
