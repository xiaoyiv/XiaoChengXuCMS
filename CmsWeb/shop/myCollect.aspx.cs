﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_myCollect : System.Web.UI.Page
{
    protected int user_id;//会员id

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Application["usercard"] = userModel.usercard;
            Bind_date(user_id);//赋值操作
        }
    }
    #region 赋值操作========================
    public void Bind_date(int user_id)
    {
        DataTable dt = new Cms.BLL.C_collect().GetList("typeId=1 and user_id=" + user_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        else
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }

        dt = new Cms.BLL.C_collect().GetList("typeId=2 and user_id=" + user_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepIsNo.DataSource = dt;
            RepIsNo.DataBind();
        }
        else
        {
            RepIsNo.DataSource = dt;
            RepIsNo.DataBind();
        }

        dt = new Cms.BLL.C_collect().GetList("typeId=3 and user_id=" + user_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepIsYes.DataSource = dt;
            RepIsYes.DataBind();
        }
        else
        {
            RepIsYes.DataSource = dt;
            RepIsYes.DataBind();
        }


    }
   /// <summary>
   /// 获取商品的路径
   /// </summary>
   /// <param name="article_id"></param>
   /// <returns></returns>
    public string getUrl(string id)
    {
        string result = "";
        if (new Cms.BLL.C_collect().GetModel(Convert.ToInt32(id)).integral.ToString() == "0")
        {
            result = "/shop/productDetail.aspx?id=" + new Cms.BLL.C_collect().GetModel(Convert.ToInt32(id)).article_id;
        }
        else
        {
            result = "/shop/integralDetail.aspx?id=" + new Cms.BLL.C_collect().GetModel(Convert.ToInt32(id)).article_id;
        }
        return result;
    }

    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    public string getState(int order_state)
    {
        string result = "";
        switch (order_state)
        {
            case 0:
                result = "马上预约";
                break;
            case 1:
                result = "已预约";
                break;
            case 2:
                result = "已领取";
                break;
        }

        return result;
    }
    #endregion
}