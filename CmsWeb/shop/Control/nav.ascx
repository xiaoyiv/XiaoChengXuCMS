﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nav.ascx.cs" Inherits="shop_Control_nav" %>
  <div class="sidebar-left">
        <div class="sidebar-scroll-left">
            <div class="sidebar-header-left">
                <img src="/shop/images/logo.png" class="sidebar-left-logo replace-2x" alt="img"/>
                <a href="javascript:void(0)" class="close-sidebar-left"></a>
            </div>
            <p class="sidebar-divider-text">
                导航菜单</p>
            <a href="/" class="nav-item">网站首页<em class="icon-page"></em></a>
            <asp:Repeater runat="server" ID="RepNav" OnItemDataBound="RepNav_ItemDataBound">
                <ItemTemplate>
                    <a href="javascript:void(0)" id="submenu-<%# Container.ItemIndex+1%>" class="nav-item">
                        <%#Eval("className")%><em class="icon-drop"></em></a>
                    <div class="submenu submenu-<%# Container.ItemIndex+1%>">
                        <asp:Repeater runat="server" ID="RepNavOne">
                            <ItemTemplate>
                                <a href="/shop/productList.aspx?classid=<%#Eval("classId")%>"><em></em>
                                    <%#Eval("className")%></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <a href="javascript:void(0)"><em class="close-bottom-left"></em></a>
        </div>
    </div>
<%--   <script type="text/javascript" src="/shop/js/main.js"></script>--%>