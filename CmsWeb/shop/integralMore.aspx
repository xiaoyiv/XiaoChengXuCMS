﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="integralMore.aspx.cs" Inherits="shop_integralMore" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater runat="server" ID="RepList">
       <ItemTemplate>
            	<li class="br0">
                	<a href="/shop/integralDetail.aspx?id=<%#Eval("articleId") %>">
                    	<img src="<%#Eval("photoUrl") %>" alt="">
                        <h3><%#Eval("title") %></h3>
                        <p><%#getIntegral(Convert.ToInt32(Eval("articleId"))) %>积分<del>¥ <%#getMprice(Convert.ToInt32(Eval("articleId"))) %></del></p>
                    </a>
                </li>
                </ItemTemplate>
                 <%-- <FooterTemplate>
                <%#RepList.Items.Count == 0 ? "<div style='text-align:center;'>暂无积分产品</div>" : ""%>
            </FooterTemplate>--%>
    </asp:Repeater>
    <div class="clear">
    </div>
    </form>
</body>
</html>
