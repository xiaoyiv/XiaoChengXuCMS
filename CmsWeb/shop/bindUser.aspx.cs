﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_bindUser : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Bind_date();

        }
    }

    #region 赋值操作========================
    public void Bind_date()
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        this.tel.Value = userModel.telphone;
        this.card.Value = userModel.usercard;
        

        this.openid.Value = userModel.openid;
       
        
    }
    #endregion

    #region 绑定会员卡==============================
    protected void Button1_Click(object sender, EventArgs e)
    {
        
            Cms.BLL.C_user cuser = new Cms.BLL.C_user();
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            //Crm会员绑定
           
            wxuser.userinfo xw = new wxuser.userinfo();
            string selectDefault = is_default.SelectedValue.ToString();
            if (selectDefault == "1")
            {
                if (this.card.Value.ToString() != "" && this.card.Value.ToString() != null)
                {
                    xw = wxuser.getUserBind(userModel.openid.ToString(), this.tel.Value.ToString(), this.card.Value.ToString());
                    if (xw.result == "绑定成功")
                    {
                        //Crm查询会员信息
                        xw = wxuser.getuserinfo(userModel.openid);
                        if (xw.result == "获取成功")
                        {
                            //获取成功更新新会
                            userModel = cuser.GetModel(userModel.id);
                            userModel.usercard = xw.usercard;
                            userModel.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString());
                            userModel.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                            userModel.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                            userModel.userlevel = xw.userlevel;
                            userModel.shopname = xw.shopname;
                            cuser.Update(userModel);
                            JscriptMsg("绑定成功！", "/shop/main.aspx", "SUCCESS");
                        }
                        else
                        {
                            //获取失败注册新会
                            //wxuser.UserSale wu = new wxuser.UserSale();
                            //wu = wxuser.getUserRegister(userModel.openid, userModel.username, userModel.sex, userModel.useraddress, string.Format("{0:yyyy-MM-dd}", userModel.birthday), string.Format("{0:yyyy-MM-dd}", userModel.marryday), userModel.telphone);
                            ////Literal1.Text = wu.content + "-" + wu.result + "-" + wu.usercard;
                            //xw = wxuser.getuserinfo(userModel.openid);
                            //userModel = cuser.GetModel(userModel.id);
                            //userModel.usercard = xw.usercard;
                            //userModel.userlevel = xw.userlevel;
                            //cuser.Update(userModel);
                            JscriptMsg("绑定失败！", "NotJump", "Error");
                        }

                    }
                    else
                    {
                        JscriptMsg("绑定失败！", "NotJump", "Error");
                    }
                }
                else
                {
                    JscriptMsg("请填写会员卡号！", "NotJump", "Error");
                }
            }
            else
            {
                //获取失败注册新会
                wxuser.UserSale wu = new wxuser.UserSale();
                wu = wxuser.getUserRegister(userModel.openid, userModel.username, userModel.sex, userModel.useraddress, string.Format("{0:yyyy-MM-dd}", userModel.birthday), string.Format("{0:yyyy-MM-dd}", userModel.marryday), userModel.telphone, userModel.shopcode);
                //Literal1.Text = wu.content + "-" + wu.result + "-" + wu.usercard;
                xw = wxuser.getuserinfo(userModel.openid);
                userModel = cuser.GetModel(userModel.id);
                userModel.usercard = xw.usercard;
                userModel.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString());
                userModel.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                userModel.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                userModel.userlevel = xw.userlevel;
                userModel.shopname = xw.shopname;
                cuser.Update(userModel);
                JscriptMsg("开通线下会员卡成功！", "/shop/main.aspx", "SUCCESS");
            }
       
    }
    #endregion
    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
    protected void is_default_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectDefault = is_default.SelectedValue.ToString();
        if (selectDefault == "0")
        {
            cardselect.Visible = false;
            cardselect.Style.Value = "display:none;";
        }
        else
        {
            cardselect.Visible = true;
            cardselect.Style.Value = "display:block;";
        }
    }
}