﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myIntegral.aspx.cs" Inherits="shop_myIntegral" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.extend({
                "jTab": function (o) {
                    o = $.extend({
                        menutag: "", 	//选项卡按钮标签
                        boxtag: "",     //选项卡内容标签
                        cur: 0, 	  	//选项卡的默认索引值
                        act: "mouseover", //事情触发的默认事件可以是"click"
                        fade: 0, 		//淡入的时间
                        auto: false, //false,true表示开关定时器
                        autoTime: 3000	//定时执行的时间
                    }, o)
                    $(o.menutag).eq(0).addClass("cur");
                    $(o.boxtag).eq(0).siblings().hide();
                    $(o.menutag).bind(o.act, function () {
                        index = $(o.menutag).index(this);
                        $(this).addClass("cur").siblings().removeClass("cur")
                        $(o.boxtag).eq(index).show(o.fade).siblings().hide();
                    })

                }
            })
        })(jQuery);
    </script>
    <script type="text/javascript">
        $(function () {
            $(".tab").jTab({
                menutag: ".tab-m>li",
                boxtag: ".tab-box>div",
                cur: 0,
                act: "mouseover",
                fade: 0,
                auto: false,
                autoTime: 3000
            })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">我的积分</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="mine-integ">
            	<div class="card-num">
                	会员卡号：<%=Application["usercard"] %>
                </div>
                <div class="dbe-borbg integ-exc">
                	<div class="tab myintegral">
                        <ul class="tab-m">
                          <li><a href="javascript:void(0)"><span>可兑换积分</span></a></li>
                          <li><a href="javascript:void(0)"><span>已兑换积分</span></a></li>
                          <div class="clear"></div>
                        </ul>
                        <div class="tab-box">
                        	<div class="thfnxw">
                            	<div class="integ-ed">
                                	<p>共<span><%= Application["userscore"]%></span></p>
                                    <p><a href="/shop/integral.aspx">兑换商品</a></p>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="thfnxw">
                            	<div class="integ-yes">
                                	共<span><%= Application["userYesScore"]%></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="integ-det dbe-borbg pd-l10 mt10">
                	<h2>获得积分明细</h2>
                    <ul class="integ-det-ul">
                    <asp:Repeater runat="server" ID="RepIntegral">
                    <ItemTemplate>
                    	<li>
                        	<h3><%#string.Format("{0:yyyy-MM-dd}", Eval("updateTime"))%></h3>
                            <img src="<%#getPhoto(Eval("article_id").ToString())%>">
                            <p><%#Eval("title")%><span><%#Eval("wescore")%></span></p>
                            <div class="clear"></div>
                        </li>
                     </ItemTemplate>
                       <FooterTemplate>
                <%#RepIntegral.Items.Count == 0 ? "<div style='text-align:center;'>暂无记录</div>" : ""%>
            </FooterTemplate>
                    </asp:Repeater>
                    </ul>
                </div>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
