﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="looseMore.aspx.cs" Inherits="shop_looseMore" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater ID="RepList" runat="server">
                <ItemTemplate>
                	<li>
                    	<a href="/shop/looseDetail.aspx?id=<%#Eval("articleId") %>">
                        	<img src="<%#Eval("photoUrl") %>">
                            <div class="search-li-r">
                                <div class="property">
                                    <p>钻重: <span><%#getLoose(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <p>颜色: <span><%#getColor(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <p>净度: <span><%#getClarity(Convert.ToInt32(Eval("articleId")))%></span></p>
                                    <p>切工: <span><%#getCut(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <div class="clear"></div>
                                </div>
                                <div class="price">
                                    ￥<%#getPrice(Convert.ToInt32(Eval("articleId"))) %>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </a>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
    </form>
</body>
</html>
