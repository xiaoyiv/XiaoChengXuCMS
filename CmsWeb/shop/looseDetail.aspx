﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="looseDetail.aspx.cs" Inherits="shop_looseDetail" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/TouchSlide.1.1.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.extend({
                "jTab": function (o) {
                    o = $.extend({
                        menutag: "", 	//选项卡按钮标签
                        boxtag: "",     //选项卡内容标签
                        cur: 0, 	  	//选项卡的默认索引值
                        act: "mouseover", //事情触发的默认事件可以是"click"
                        fade: 0, 		//淡入的时间
                        auto: false, //false,true表示开关定时器
                        autoTime: 3000	//定时执行的时间
                    }, o)
                    $(o.menutag).eq(0).addClass("cur");
                    $(o.boxtag).eq(0).siblings().hide();
                    $(o.menutag).bind(o.act, function () {
                        index = $(o.menutag).index(this);
                        $(this).addClass("cur").siblings().removeClass("cur")
                        $(o.boxtag).eq(index).show(o.fade).siblings().hide();
                    })

                }
            })
        })(jQuery);
    </script>
    <script type="text/javascript">
        $(function () {
            $(".tab").jTab({
                menutag: ".tab-m>li",
                boxtag: ".tab-box>div",
                cur: 0,
                act: "mouseover",
                fade: 0,
                auto: false,
                autoTime: 3000
            })
        })
    </script>
    <script src="/shop/js/jquery-ui-min.js" type="text/javascript"></script>
    <script src="/shop/js/custom.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:void(0);" class="nav"></a>
        <a href="/shop/cart.aspx" class="shopping"></a>
        <a href="javascript:void(0);" class="page-title">产品中心</a>
    </header>
    <section class="main main-product main1">
        <div class="p-contain">
        	<div class="p-product-det">
            	<div class="p-product-det1">
                	<div class="p-product-det-img">
                		<div id="slideBox" class="slideBox">
                            <div class="bd">
                                <ul>
                                   <asp:Repeater ID="RepeaterPhoto" runat="server">
                                    <ItemTemplate>
                                    <li><a class="pic" href="javascript:void(0)"><img src="<%#Eval("thumb_path")%>" /></a></li>
                                    </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                            <div class="hd">
                                <ul></ul>
                            </div>
                        </div>
                        <script type="text/javascript">
                            TouchSlide({
                                slideCell: "#slideBox",
                                titCell: ".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
                                mainCell: ".bd ul",
                                effect: "leftLoop",
                                autoPage: true, //自动分页
                                autoPlay: true //自动播放
                            });
                        </script>
                    </div>
                    <div class="p-product-det-name">
                    	<div class="product-det-namel">
                        	<h3><%=Application["title"]%></h3>
                            <p><span><i>优惠价：</i><b>¥</b><%=Application["price"]%></span><samp><em>会员更优惠</em></samp></p>
                            <del>市场价：<%=Application["marketPrice"]%></del>
                        </div>
                        <asp:LinkButton ID="LinkButton1" CssClass="det-collect" runat="server" 
                            onclick="LinkButton1_Click">收藏</asp:LinkButton>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="p-product-detail">
                    <div class="p-product-add">
                    	<span>数量：</span>
                        <div class="num">
                             <input id="min2" name="" type="button" value="-" style=" float:left; width:43px; height:39px; border:none; background:#eaeaea; color:#b1b1b1; cursor:pointer; font-weight:bold;-webkit-appearance : none ; border-radius:0;" />
                            <input id="text_box2" runat="server" name="" readonly="readonly" type="text" value="1" style=" float:left; width:65px; height:39px; line-height:39px; text-align:center; border:none; border-left:1px #d0d0d0 solid; border-right:1px #d0d0d0 solid; background:none; font-family:Arial; color:#333;-webkit-appearance : none ; border-radius:0;" />
                            <input id="add2" name="" type="button" value="+" style=" float:left; width:43px; height:39px;border:none; color:#b1b1b1; background:#eaeaea; cursor:pointer; font-weight:bold;-webkit-appearance : none ; border-radius:0;" />
                            <div class="clear"></div>
                        </div>
                       <p>(库存<%=Application["stock"] %>)</p>
                       <div class="clear"></div>
                        <i>送积分：<%=Application["integral"]%></i>
                        <script type="text/javascript">
                            $(function () {
                                var t = $("#text_box2");
                                $("#add2").click(function () {
                                    t.val(parseInt(t.val()) + 1)
                                })
                                $("#min2").click(function () {
                                    if (t.val() > 1) {
                                        t.val(parseInt(t.val()) - 1)
                                    }
                                })
                            })
						</script>
                    </div>
                </div>
                <div class="p-product-pic">
                	<div class="tab">
                        <ul class="tab-m">
                          <li><a href="javascript:void(0)"><span>图文详情</span></a></li>
                          <li><a href="javascript:void(0)"><span>商品属性</span></a></li>
                          <li><a href="javascript:void(0)"><span>购买记录</span></a></li>
                          <div class="clear"></div>
                        </ul>
                        <div class="tab-box">
                        	<div class="thfnxw">
                            	<div class="detail-img">
                                	<%=Application["content"]%>
                                </div>
                            </div>
                            <div class="thfnxw">
                            	<div class="pro-attr">
                                    <ul class="ul-3">
                                       <%if (Application["style"] != "")
                                          {%><li><span>款&nbsp;&nbsp;号</span><p><%=Application["style"]%></p></li><% } %>                           
                                        <%if (Application["type"] != "")
                                          {%><li><span>类型</span><p><%=Application["type"]%></p></li><%}%>              
                                        <%if (Application["kim_joong"] != "")
                                          {%><li><span>金重</span><p><%=Application["kim_joong"]%></p></li> <% } %> 
                                        <%if (Application["inch_number"] != "")
                                          {%><li><span>寸数</span><p><%=Application["inch_number"]%></p></li> <% } %> 
                                        <%if (Application["gold_price"] != "")
                                          {%><li><span>金价</span><p><%=Application["gold_price"]%></p></li> <% } %> 
                                        <%if (Application["labor_charges"] != "")
                                          {%><li><span>工费</span><p><%=Application["labor_charges"]%></p></li> <% } %>                                  
                                        <%if (Application["color"] != "")
                                          {%><li><span>颜色</span><p><%=Application["color"]%></p></li> <% } %> 
                                        <%if (Application["clarity"] != "")
                                          {%><li><span>净度</span><p><%=Application["clarity"]%></p></li>    <% } %> 
                                        <%if (Application["cut"] != "")
                                          {%><li><span>切工</span><p><%=Application["cut"]%></p></li>   <% } %> 
                                        <%if (Application["main_stone_weight"] != "")
                                          {%><li><span>主石重</span><p><%=Application["main_stone_weight"]%></p></li>  <% } %> 
                                        <%if (Application["main_stone_number"] != "")
                                          {%><li><span>主石数量</span><p><%=Application["main_stone_number"]%></p></li>  <% } %> 
                                        <%if (Application["total_weight"] != "")
                                          {%><li><span>总重</span><p><%=Application["total_weight"]%></p></li>  <% } %> 
                                        <%if (Application["sales_labor_charge"] != "")
                                          {%><li><span>销售工费</span><p><%=Application["sales_labor_charge"]%></p></li>  <% } %> 
                                        <%if (Application["accessories_price"] != "")
                                          {%><li><span>配件价</span><p><%=Application["accessories_price"]%></p></li>   <% } %> 
                                        <%if (Application["product_category"] != "")
                                          {%><li><span>产品种类</span><p><%=Application["product_category"]%></p></li> <% } %> 
                                        <%if (Application["auxiliary_stone_weight"] != "")
                                          {%><li><span>辅石重</span><p><%=Application["auxiliary_stone_weight"]%></p></li> <% } %> 
                                        <%if (Application["side_stone_number"] != "")
                                          {%><li><span>辅石数</span><p><%=Application["side_stone_number"]%></p></li> <% } %> 
                                        <%if (Application["certificate_no"]!= "")
                                          {%><li><span>证书号</span><p><%=Application["certificate_no"]%></p></li>  <% } %> 
                                          <%if (Application["material"] != "")
                                          {%><li><span>金属材质</span><p><%=Application["material"]%></p></li>  <% } %> 
                                          <%if (Application["bstype"] != "")
                                          {%><li><span>宝石类</span><p><%=Application["bstype"]%></p></li>  <% } %> 
                                          <%if (Application["pintype"] != "")
                                          {%><li><span>品类</span><p><%=Application["pintype"]%></p></li>  <% } %> 
                                    </ul>
                                </div>
                            </div>
                            <div class="thfnxw">
                            	<div class="tran-records">
                                	<table border="0" cellspacing="0" cellpadding="0" class="records-table">
                                      <tr>
                                        <th>买家</th>
                                        <th style=" text-align:left;">款式/型号</th>
                                        <th>成交时间</th>
                                      </tr>
                                      <asp:Repeater runat="server" ID="RepBuyRecord">
                                      <ItemTemplate>
                                      <tr>
                                        <td><%#new Cms.BLL.C_user().GetModel(Convert.ToInt32(Eval("user_id"))).username%></td>
                                        <td style=" text-align:left;"><%#Eval("property_value")%> </td>
                                        <td><span><%#Eval("updateTime")%></span></td>
                                      </tr>
                                      </ItemTemplate>
                                      </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-button">
                	 <input type="hidden" id="article_id" runat="server" value="" />
                <asp:Button ID="Button1" runat="server" CssClass="buy-button" Text="立即购买" 
                        onclick="Button1_Click"></asp:Button>
                       
              <asp:Button ID="Button3"  runat="server" CssClass="add-button" Text="加入购物车" onclick="Button3_Click"></asp:Button>
                </div>
               
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    <uc1:nav ID="nav1" runat="server" />
    </form>
</body>
</html>
