﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class go : System.Web.UI.Page
{
    //模拟的奖池
    public IList<Prize> prize_arr
    {
        get {
            return new List<Prize>{
                new Prize{pid = 1, min =new int[]{1},max=new int[]{29},odds = 1,prize = "一等奖"},
                new Prize{pid = 2, min =new int[]{302},max=new int[]{328},odds = 2,prize = "二等奖"},
                new Prize{pid = 3, min =new int[]{242},max=new int[]{268},odds = 5,prize = "三等奖"},
                new Prize{pid = 4, min =new int[]{182},max=new int[]{208},odds = 7,prize = "四等奖"},
                new Prize{pid = 5, min =new int[]{122},max=new int[]{148},odds = 10,prize = "五等奖"},
                new Prize{pid = 6, min =new int[]{62},max=new int[]{88},odds = 10,prize = "六等奖"},
                new Prize{pid = 7, min =new int[]{32,92,152,212,272,332},max=new int[]{58,118,178,238,298,358},odds = 50,prize = "七等奖"},
            };
        }
    }

    public int getRand(List<int> proArr)
    {
        int result = 0;
        var proSum = proArr.Sum();
        Random ranDom = new Random();
        for (var i = 0; i < proArr.Count; i++)
        {
            var ranNum = ranDom.Next(1, proSum);
            if (ranNum < proArr[i])
            {
                result = i + 1;
            }
            else {
                proSum -= proArr[i];
            }
        }
        return result;
    }
     

    protected void Page_Load(object sender, EventArgs e)
    {
        var proArr = new List<int>();//几率的集合
        foreach (var i in prize_arr) {
            proArr.Add(i.odds);
        }
        var rid = getRand(proArr);//中奖id
        var res = prize_arr[rid - 1];//中奖项
        var min = res.min;
        var max = res.max;
        var angle = 0;
        var prize = string.Empty;
        var ranDom = new Random();
        //如果此奖项在转盘上有多个格子
        if (min.Length > 1 && max.Length > 1)
        {
            var i = ranDom.Next(0, 5);
            angle = ranDom.Next(min[i], max[i]);
        }
        else {
            angle = ranDom.Next(min[0], max[0]);
        }
        prize = res.prize;
        Response.Write("{\"angle\" : " + angle + ",\"prize\" : \"" + prize + "\"}");
        Response.End();
    }

    public class Prize
    { 
        public int pid {get;set;}//奖项编号id
        public int[] min{get;set;}//最小转动角度
        public int[] max{get;set;}//最大转动角度
        public string prize{get;set;}//奖项名称
        public int odds {get;set;}//概率
    }

}