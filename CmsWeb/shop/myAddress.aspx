﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myAddress.aspx.cs" Inherits="shop_myAddress" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="format-detection" content="telephone=no"/>
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/customInput.jquery.js"></script>
</head>
<body>
    <form id="form1" runat="server">
<section class="all">
    <header>
       <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">收货地址</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="p-address">
                <ul>
                <asp:Repeater runat="server" ID="RepAddress">
                    <ItemTemplate>
                	<li>
                    <div class="custom-radio">
                       <asp:RadioButton ID="RadioButton1" runat="server" 
                            Checked='<%#bool.Parse(Eval("is_default").ToString()=="1"?"True":"False")%>' 
                            oncheckedchanged="RadioButton1_CheckedChanged" AutoPostBack="true"></asp:RadioButton>
                            </div>
                            <asp:HiddenField
                        ID="Fielddocid" Value='<%#Eval("id")%>' runat="server" />
                            <div class="bill-pay-li">
                                <h3>收货人：<%#Eval("consignee")%></h3>
                                <p><span runat="server" visible='<%#bool.Parse(Eval("is_default").ToString()=="1"?"True":"False")%>'>[默认地址]</span><%#Eval("address")%></p>
                                <b><%#Eval("cellphone")%></b>
                            </div>
                      
                        <a href="/shop/EditAddress.aspx?action=edit&id=<%#Eval("id")%>" class="address_edit"></a>
                        <div class="clear"></div>
                    </li>
                     </ItemTemplate>
                      <FooterTemplate>
                <%#RepAddress.Items.Count == 0 ? "<div style='text-align:center;'>暂无记录</div>" : ""%>
            </FooterTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="product-button1">
                <a href="/shop/EditAddress.aspx?action=add" class="login_ipt mt0">新增收货地址</a>
            </div>
        </div>
             
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
<script type="text/javascript">    $('input').customInput();</script>
    </form>
</body>
</html>
