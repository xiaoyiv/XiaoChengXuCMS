﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_orderDetail : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            
            int orderId = Convert.ToInt32(Request["id"].ToString());
            Bind_date(orderId);//赋值操作
            Bind_address(user_id);
        }
    }

    #region 收货地址赋值=======================
    /// <summary>
    /// 收货地址赋值
    /// </summary>
    /// <param name="user_id"></param>
    public void Bind_address(int user_id)
    {
        DataSet ds = new Cms.BLL.c_user_address().GetList(1, "is_default=1 and user_id=" + user_id, "id desc");
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepAddress.DataSource = ds.Tables[0].DefaultView;
            RepAddress.DataBind();
           
        }
        else
        {
            RepAddress.DataSource = null;
            RepAddress.DataBind();
        }

    }
    #endregion
    #region 赋值操作========================
    public void Bind_date(int orderId)
    {
        Cms.Model.C_order model = new Cms.BLL.C_order().GetModel(orderId);
        Application["updateTime"] = model.updateTime.ToString();
        Application["order_num"] = model.order_num.ToString();
        Application["quantity_sum"] = model.quantity_sum.ToString();
        Application["price_sum"] =Convert.ToDecimal(model.price_sum).ToString("0.00");
        Application["is_payment"] = model.is_payment.ToString();
        Application["is_refund"] = model.is_refund.ToString();
        Application["is_delivery"] = model.is_delivery.ToString();
        Application["is_transaction"] = model.is_transaction.ToString();
        Application["id"] = model.id.ToString();

        DataSet dsNavOne = new Cms.BLL.C_ordersub().GetList("order_id=" + orderId + " order by id desc");
        if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
        {
            RepOrderListSub.DataSource = dsNavOne.Tables[0].DefaultView;
            RepOrderListSub.DataBind();
        }  
    }

   
    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    public string getIsRefund(string IsRefund, string orderId)
    {
        string result = "";
        Cms.Model.C_order model = new Cms.BLL.C_order().GetModel(Convert.ToInt32(orderId));
        if (model.is_payment == 1)
        {
            if (IsRefund == "0")
            {
                result = "<a href='/shop/Refund.aspx?orderid=" + orderId + "'>申请退款</a>";
            }
            else if (IsRefund == "1")
            {
                result = "<a href='javascript:void(0);'>审核退款中</a>";
            }
            else if (IsRefund == "2")
            {
                result = "<a href='/api/payment/clientRefund.aspx?orderid=" + orderId + "'>确认退款</a>";
            }
            else if (IsRefund == "3")
            {
                result = "<a href='javascript:void(0);'>退款成功</a>";
            }
        }


        return result;
    }
    public string getIsDelivery(string IsDelivery, string orderId)
    {
        string result = "";
        if (IsDelivery == "0")
        {
            result = "<a href='javascript:void(0);'>未发货</a>";
        }
        else if (IsDelivery == "1")
        {
            int is_receiving = Convert.ToInt32(new Cms.BLL.C_order().GetModel(Convert.ToInt32(orderId)).is_receiving);
            if (is_receiving == 1)
            {
                result = "<a href='javascript:void(0);'>已收货</a>";
            }
            else
            {
                result = "<a href='/shop/Delivery.aspx?orderid=" + orderId + "'>确认收货</a>";
            }
        }


        return result;
    }
    #endregion
}