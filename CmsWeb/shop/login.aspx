﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="shop_login" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="all">
    <header>
        <a href="javascript:void(0)" onclick="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">会员登录</a>
    </header>

    <section class="main main-product">
        <div class="p-contain">
        	<div class="p-vip">
            	<p>尊敬的会员请您填写正确的手机号码！</p>
                <div class="vip-input">
                	<input name="" id="telPhone" runat="server" type="text" placeholder="请输入手机号码" onblur="getPhone();"/>
                  
                </div>
                
                 <%--<div class="new-vip">
                	<a href="newUser.aspx">点击开通新会员</a>
                </div>--%>
                <div class="product-button1">
                    <input id="HiddenField1" type="hidden" value="" runat="server" />
                     <input id="HiddenField2" type="hidden" value="" runat="server" />
                <asp:Button ID="Button1" runat="server"  CssClass="login_iptl" onclick="Button1_Click" ValidationGroup="submit" CausesValidation="True"  Text="登录"></asp:Button>
                <asp:Button ID="Button2" runat="server"  CssClass="login_iptr" onclick="Button2_Click" ValidationGroup="submit" CausesValidation="True"  Text="注册"></asp:Button>
                </div>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
    </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
<script type="text/javascript">
    function getPhone() {
        var telPhone = $("#telPhone").val();
        $.ajax({
            type: "POST",
            url: "ashx/shop.ashx?action=getTelphone&telphone=" + telPhone,
            data: "",
            success: function (msg) {
                if (msg == "1") {
                    jsprintWeb("该手机属于会员,请登录！", "NotJump", "Success");
                }
                else if (msg == "2") {
                    jsprintWeb("手机号不能为空！", "NotJump", "Error");
                }
                else if (msg == "3") {
                    jsprintWeb("请输入有效手机号码！", "NotJump", "Error");
                }
                else {

                    jsprintWeb("该手机未成为会员,请开通新会员！", "NotJump", "Error");
                }
            }
        });
    }
</script>
</html>
