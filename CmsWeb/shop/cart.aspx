﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cart.aspx.cs" Inherits="shop_cart" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/customInput.jquery.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
    <script type="text/javascript">
	$(function() {
		$(".p-contain").css("height", $(window).height() - 100 );
	 });
	 
</script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="delete" onclick="LinkButton1_Click"></asp:LinkButton>
        <a href="/shop/cart.aspx" class="page-title">购物车</a>
    </header>
   <%-- <div class="cd-popup" role="alert">
        <div class="cd-popup-container">
            <div class="delete-goods">
                <h2>确定从购物车删除商品？</h2>
                <a href="#" class="delete-reset">删除</a>
                <a href="#" class="delete-reset">取消</a>
            </div>
        </div> <!-- cd-popup-container -->
    </div>--%>

    <section class="main main-product">
        <div class="p-contain">
        	<div class="gwc_list gwc_tb2">
                <ul>
                <asp:Repeater runat="server" ID="RepList" OnItemCommand="RepList_ItemCommand">
                <ItemTemplate>
                    <li>
                        <div class="gwc_lil">
                           <asp:CheckBox ID="Check_Select" CssClass="checkall" runat="server" Style="vertical-align: middle;" Checked='<%#bool.Parse(Eval("is_checked").ToString()=="2"?"True":"False")%>'
                                oncheckedchanged="Check_Select_CheckedChanged" AutoPostBack="true" />
                                <asp:HiddenField
                        ID="Fielddocid" Value='<%#Eval("id")%>' runat="server" />
                         <asp:HiddenField
                        ID="HiddenCheck" Value='<%#Eval("is_checked")%>' runat="server" />
                        </div>
                        <div class="gwc_lir">
                        	<a href="/shop/productDetail.aspx?id=<%#Eval("article_id")%>" class="gwc-img"><img src="<%#getPhoto(Eval("article_id").ToString())%>"></a>
                            <div class="gwc-li-r">
                                <h2><a href="/shop/productDetail.aspx?id=<%#Eval("article_id")%>"></a></h2>
                                <b>￥<%#Convert.ToDecimal(Eval("price")).ToString("0.00")%></b><div class="num1">
                                   <%-- <input id="min1" name="" class="min1" type="button" value="-" style=" float:left; width:33px; height:33px; border:none; background:none; color:#8b8b8b; cursor:pointer;" />
                                    <input id="text_box1" name="" class="text_box1" type="text" value="1" style=" float:left; width:50px; height:33px; line-height:33px; text-align:center; border:none; border-left:1px #bcbcbc solid; border-right:1px #bcbcbc solid; background:none; font-family:Arial; color:#333;" />
                                    <input id="add1" name="" class="add1" type="button" value="+" style=" float:left; width:33px; height:33px;border:none; color:#8b8b8b; background:none; cursor:pointer;" />--%>
                                     <asp:LinkButton ID="lbtnIsLess" CommandName="lbtnLess" runat="server" Style="line-height:33px; text-align:center; float:left; width:33px; height:33px; border:none; background:none; color:#8b8b8b; cursor:pointer;" ToolTip='-' Text="-" />
                                     <asp:TextBox ID="txtproquantity" runat="server" Text='<%#Eval("quantity") %>' Style=" text-align:center; float:left; width:50px; height:33px; line-height:33px; text-align:center; border:none; border-left:1px #bcbcbc solid; border-right:1px #bcbcbc solid; background:none; font-family:Arial; color:#333; border-radius:0;"></asp:TextBox>
                                     <asp:LinkButton ID="lbtnIsPlus" CommandName="lbtnAdd" runat="server" Style="line-height:33px; text-align:center; float:left; width:33px; height:33px;border:none; color:#8b8b8b; background:none; cursor:pointer;" ToolTip='+' Text="+" />
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </li>
                 </ItemTemplate>
                  <FooterTemplate>
                <%#RepList.Items.Count == 0 ? "<div style='text-align:center;'>暂无购物记录</div>" : ""%>
               
            </FooterTemplate>
                </asp:Repeater>
              
                </ul>
            </div>
            <div class="gwc_js">
                <div class="gwc_js1">
                    <div class="gwc_jsl">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="all" OnClientClick="checkAll(this)" 
                            onclick="LinkButton2_Click"><i></i><span id="checkBoxAll" runat="server">全选</span></asp:LinkButton>
                    <%--<input type="checkbox" value="" id="check-1"  class="checkedCtrl" name="check-1"/><label for="check-1">全选</label>--%><div class="clear"></div>
                    </div>
                   <asp:LinkButton ID="LinkButton3" CssClass="gwc_ipt" runat="server" onclick="LinkButton3_Click">结算</asp:LinkButton>
                    <div class="gwc_jsc">
                        合计：<b>￥<%=dTotal.ToString("0.00")%></b><span>不含运费</span>
                    </div>
                </div>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
        </section>
            </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $(".num1 .add1").click(function () {
                var t = $(".text_box1");
                t.val(parseInt(t.val()) + 1)
            })
            $(".num1 .min1").click(function () {
                var t = $(".text_box1");
                if (t.val() > 1) {
                    t.val(parseInt(t.val()) - 1)
                }
            })
        })
    </script>
    <script type="text/javascript">
        //全选check框事件
        $(".checkedCtrl").click(function () {
            var thisChecked = $(this).attr("checked");
            $("input[name='check-1']").each(function () {
                if ($(this).attr("disabled") !== 'disabled') {
                    if (thisChecked === 'checked') {
                        $(this).attr('checked', 'checked');
                    } else {
                        $(this).removeAttr('checked');
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">        $('input').customInput();</script>
    <script type="text/javascript" src="js/main.js"></script>
    </form>
</body>
</html>
