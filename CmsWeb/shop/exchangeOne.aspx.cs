﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_exchangeOne : System.Web.UI.Page
{
    protected int user_id;//会员id
    public Double dTotal = 0.00; //总计款额
    public int QTotal = 0; //总计数量
    public int dintegral;//所得总积分
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user user_model = adminUser.GetuserLoginState();//验证用户是否登录
            user_id = user_model.id;
            Application["usercard"] = user_model.usercard;
            Application["userscore"] = user_model.userscore;
            bind_exchange(user_id);
           
        }
    }
    #region 赋值操作============
    public void bind_exchange(int user_id)
    {
        DataTable dt = new Cms.BLL.C_exchange_cart().GetList("user_id="+user_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepExchange.DataSource = dt;
            RepExchange.DataBind();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dTotal += Convert.ToDouble(dt.Rows[i]["price"]) * Convert.ToInt32(dt.Rows[i]["quantity"]);
                QTotal += Convert.ToInt32(dt.Rows[i]["quantity"]);
                dintegral += Convert.ToInt32(dt.Rows[i]["quantity"]) * Convert.ToInt32(dt.Rows[i]["integral"]);
            }
        }
        else
        {
            RepExchange.DataSource = dt;
            RepExchange.DataBind();
        }
    }
    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }
    /// <summary>
    /// 获取商品积分
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getIntegral(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["userscore"].ToString();
            }
        }
        return result;
    }
    /// <summary>
    /// 获取商品市场价格
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getMprice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            }
        }
        return result;
    }
    #endregion

    
    protected void RepExchange_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        
    }


    #region 取消操作=================
    /// <summary>
    /// 取消操作
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Cms.Model.C_user user_model = adminUser.GetuserLoginState();//验证用户是否登录
        int result = Cms.DBUtility.DbHelperSQL.ExecuteSql("delete from C_exchange_cart where user_id=" + user_model.id);
        if (result > 0)
        {
            JscriptMsg("取消成功！", "/shop/integral.aspx", "Success");
        }
        else
        {
            JscriptMsg("取消失败！", "NotJump", "Error");
        }
    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion

    #region 下一步操作================
    /// <summary>
    /// 下一步操作
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string X = HiddenX.Text;
        string Y = HiddenY.Text;
       Response.Redirect("/shop/exchangeTwo.aspx?x=" + X + "&y=" + Y);
    }
    #endregion
}