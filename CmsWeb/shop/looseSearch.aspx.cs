﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class shop_looseSearch : System.Web.UI.Page
{
  public  DataTable SearchTable = new DataTable();  //声明一个DataTable对象
  public DataRow drtable;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Bind_shape();//绑定钻石形状分类
            this.Bind_color();//绑定颜色分类
            this.Bind_clarity();//绑定净度分类
            this.Bind_cut();//绑定切工分类
            if (Session["SearchTable"] != null) //判断是否为第一次添加搜索钻石否则创建DataTable
            {
                SearchTable.Columns.Add(new DataColumn("index", typeof(string)));//钻石形状
                SearchTable.Columns.Add(new DataColumn("shape", typeof(string)));//钻石形状
                SearchTable.Columns.Add(new DataColumn("price", typeof(string)));//价格
                SearchTable.Columns.Add(new DataColumn("stone", typeof(string)));//重量
                SearchTable.Columns.Add(new DataColumn("color", typeof(string)));//颜色
                SearchTable.Columns.Add(new DataColumn("clarity", typeof(string)));//净度
                SearchTable.Columns.Add(new DataColumn("cut", typeof(string)));//切工
                Session["SearchTable"] = SearchTable;
            }
            else
            {
                SearchTable = (DataTable)Session["SearchTable"];
            }

        }

    }
    #region 绑定钻石形状分类=======================================
    public void Bind_shape()
    {
        shape.Items.Clear();
        Cms.BLL.C_article_category bllcolumn = new Cms.BLL.C_article_category();
        Cms.Model.C_article_category modelcolumn = new Cms.Model.C_article_category();
        DataSet ds = bllcolumn.GetList("parent_id=0 and channel_id=5");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                ListItem item = new ListItem();
                item.Text = "" + dr["title"].ToString();
                item.Value = dr["id"].ToString();
                item.Attributes["src"] = dr["img_url"].ToString();
                item.Attributes["id"] ="test"+ dr["id"].ToString();
                shape.Items.Add(item);
                ChileNodeBindbard(dr, shape, 2);
            }
        }
    }

    private void ChileNodeBindbard(DataRow drr, CheckBoxList parentId, int m)
    {
        Cms.BLL.C_article_category bllcolumn = new Cms.BLL.C_article_category();
        Cms.Model.C_article_category modelcolumn = new Cms.Model.C_article_category();
        DataSet dss = bllcolumn.GetList("parent_id=" + drr["id"] + "and channel_id=5");
        if (dss.Tables[0].Rows.Count > 0)
        {

            string s = System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            for (int j = 1; j <= m; j++)
            {
                s += System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            }
            for (int k = 0; k < dss.Tables[0].Rows.Count; k++)
            {
                DataRow dro = dss.Tables[0].Rows[k];
                string flag = "├";
                if (dss.Tables[0].Rows.Count == 1)
                {
                    flag = "├";
                }
                else
                {
                    if (k == 0)
                    {
                        flag = "├";
                    }
                    if (k == dss.Tables[0].Rows.Count - 1)
                    {
                        flag = "├";
                    }
                }
                ListItem item = new ListItem();
                item.Text = s + flag + dro["title"].ToString();
                item.Value = dro["id"].ToString();
                item.Attributes["src"] = dro["img_url"].ToString();
                parentId.Items.Add(item);

                ChileNodeBindbard(dro, parentId, m + 5);
            }
        }
    }
    #endregion

    #region 绑定颜色分类=======================================
    public void Bind_color()
    {
        color.Items.Clear();
        DataSet ds = new Cms.BLL.C_article_category().GetList("parent_id=0 and channel_id=1");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                ListItem item = new ListItem();
                item.Text = "" + dr["title"].ToString();
                item.Value = dr["id"].ToString();
                color.Items.Add(item);
                ChileNodeBindcolor(dr, color, 2);
            }
        }
    }

    private void ChileNodeBindcolor(DataRow drr, CheckBoxList parentId, int m)
    {
        DataSet dss = new Cms.BLL.C_article_category().GetList("parent_id=" + drr["id"] + "and channel_id=1");
        if (dss.Tables[0].Rows.Count > 0)
        {

            string s = System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            for (int j = 1; j <= m; j++)
            {
                s += System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            }
            for (int k = 0; k < dss.Tables[0].Rows.Count; k++)
            {
                DataRow dro = dss.Tables[0].Rows[k];
                string flag = "├";
                if (dss.Tables[0].Rows.Count == 1)
                {
                    flag = "├";
                }
                else
                {
                    if (k == 0)
                    {
                        flag = "├";
                    }
                    if (k == dss.Tables[0].Rows.Count - 1)
                    {
                        flag = "├";
                    }
                }
                ListItem item = new ListItem();
                item.Text = s + flag + dro["title"].ToString();
                item.Value = dro["id"].ToString();
                item.Attributes["src"] = dro["img_url"].ToString();
                parentId.Items.Add(item);

                ChileNodeBindbard(dro, parentId, m + 5);
            }
        }
    }
    #endregion

    #region 绑定净度分类=======================================
    public void Bind_clarity()
    {
        clarity.Items.Clear();
        DataSet ds = new Cms.BLL.C_article_category().GetList("parent_id=0 and channel_id=2");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                ListItem item = new ListItem();
                item.Text = "" + dr["title"].ToString();
                item.Value = dr["id"].ToString();
                clarity.Items.Add(item);
                ChileNodeBindclarity(dr, clarity, 2);
            }
        }
    }

    private void ChileNodeBindclarity(DataRow drr, CheckBoxList parentId, int m)
    {
        DataSet dss = new Cms.BLL.C_article_category().GetList("parent_id=" + drr["id"] + "and channel_id=2");
        if (dss.Tables[0].Rows.Count > 0)
        {

            string s = System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            for (int j = 1; j <= m; j++)
            {
                s += System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            }
            for (int k = 0; k < dss.Tables[0].Rows.Count; k++)
            {
                DataRow dro = dss.Tables[0].Rows[k];
                string flag = "├";
                if (dss.Tables[0].Rows.Count == 1)
                {
                    flag = "├";
                }
                else
                {
                    if (k == 0)
                    {
                        flag = "├";
                    }
                    if (k == dss.Tables[0].Rows.Count - 1)
                    {
                        flag = "├";
                    }
                }
                ListItem item = new ListItem();
                item.Text = s + flag + dro["title"].ToString();
                item.Value = dro["id"].ToString();
                item.Attributes["src"] = dro["img_url"].ToString();
                parentId.Items.Add(item);

                ChileNodeBindbard(dro, parentId, m + 5);
            }
        }
    }
    #endregion

    #region 绑定切工分类=======================================
    public void Bind_cut()
    {
        cut.Items.Clear();
        DataSet ds = new Cms.BLL.C_article_category().GetList("parent_id=0 and channel_id=4");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                ListItem item = new ListItem();
                item.Text = "" + dr["title"].ToString();
                item.Value = dr["id"].ToString();
                cut.Items.Add(item);
                ChileNodeBindcut(dr, cut, 2);
            }
        }
    }

    private void ChileNodeBindcut(DataRow drr, CheckBoxList parentId, int m)
    {
        DataSet dss = new Cms.BLL.C_article_category().GetList("parent_id=" + drr["id"] + "and channel_id=4");
        if (dss.Tables[0].Rows.Count > 0)
        {

            string s = System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            for (int j = 1; j <= m; j++)
            {
                s += System.Web.HttpContext.Current.Server.HtmlDecode("&nbsp;");
            }
            for (int k = 0; k < dss.Tables[0].Rows.Count; k++)
            {
                DataRow dro = dss.Tables[0].Rows[k];
                string flag = "├";
                if (dss.Tables[0].Rows.Count == 1)
                {
                    flag = "├";
                }
                else
                {
                    if (k == 0)
                    {
                        flag = "├";
                    }
                    if (k == dss.Tables[0].Rows.Count - 1)
                    {
                        flag = "├";
                    }
                }
                ListItem item = new ListItem();
                item.Text = s + flag + dro["title"].ToString();
                item.Value = dro["id"].ToString();
                item.Attributes["src"] = dro["img_url"].ToString();
                parentId.Items.Add(item);

                ChileNodeBindbard(dro, parentId, m + 5);
            }
        }
    }
    #endregion

    #region 重置条件=============
    protected void Button1_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < shape.Items.Count; i++)
        {
            shape.Items[i].Selected = false;
        }
        Response.Redirect("/shop/looseSearch.aspx");
    }

    #endregion

    #region 搜索钻石==========
    protected void Button2_Click(object sender, EventArgs e)
    {
        #region 钻石形状
        StringBuilder sbuBuilder_shape = new StringBuilder();
        for (int i = 0; i < shape.Items.Count; i++)
        {
            if (shape.Items[i].Selected == true)
            {
                sbuBuilder_shape.Append(shape.Items[i].Value + ",");
            }
        }
        if (sbuBuilder_shape.Length > 1)
        {
            sbuBuilder_shape.Remove(sbuBuilder_shape.Length - 1, 1);
        }
        #endregion

        #region 颜色形状
        StringBuilder sbuBuilder_color = new StringBuilder();
        for (int i = 0; i < color.Items.Count; i++)
        {
            if (color.Items[i].Selected == true)
            {
                sbuBuilder_color.Append(color.Items[i].Value + ",");
            }
        }
        if (sbuBuilder_color.Length > 1)
        {
            sbuBuilder_color.Remove(sbuBuilder_color.Length - 1, 1);
        }
        #endregion

        #region 净度
        StringBuilder sbuBuilder_clarity = new StringBuilder();
        for (int i = 0; i < clarity.Items.Count; i++)
        {
            if (clarity.Items[i].Selected == true)
            {
                sbuBuilder_clarity.Append(clarity.Items[i].Value + ",");
            }
        }
        if (sbuBuilder_clarity.Length > 1)
        {
            sbuBuilder_clarity.Remove(sbuBuilder_clarity.Length - 1, 1);
        }
        #endregion

        #region 切工
        StringBuilder sbuBuilder_cut = new StringBuilder();
        for (int i = 0; i < cut.Items.Count; i++)
        {
            if (cut.Items[i].Selected == true)
            {
                sbuBuilder_cut.Append(cut.Items[i].Value + ",");
            }
        }
        if (sbuBuilder_cut.Length > 1)
        {
            sbuBuilder_cut.Remove(sbuBuilder_cut.Length - 1, 1);
        }
        #endregion

        string price = range_price.Value;
        string stone = range_carat.Value;
       //InsertList(sbuBuilder_shape.ToString(), price, stone, sbuBuilder_color.ToString(), sbuBuilder_clarity.ToString(), sbuBuilder_cut.ToString());

        Response.Redirect("/shop/looseList.aspx?shape=" + sbuBuilder_shape.ToString() + "&price=" + price + "&stone=" + stone + "&color=" + sbuBuilder_color.ToString() + "&clarity=" + sbuBuilder_clarity.ToString() + "&cut=" + sbuBuilder_cut.ToString());
    }
    #endregion

    #region 向DataTable插入数据，并判断是否为同一种，如果是则让数量增一，否则创建一行======================================
    public void InsertList(string shape, string price, string stone, string color, string clarity, string cut)
    {
        if (Session["SearchTable"] != null) //判断是否为第一次添加搜索钻石否则创建DataTable
        {
            SearchTable.Columns.Add(new DataColumn("index", typeof(string)));//钻石形状
            SearchTable.Columns.Add(new DataColumn("shape", typeof(string)));//钻石形状
            SearchTable.Columns.Add(new DataColumn("price", typeof(string)));//价格
            SearchTable.Columns.Add(new DataColumn("stone", typeof(string)));//重量
            SearchTable.Columns.Add(new DataColumn("color", typeof(string)));//颜色
            SearchTable.Columns.Add(new DataColumn("clarity", typeof(string)));//净度
            SearchTable.Columns.Add(new DataColumn("cut", typeof(string)));//切工
            Session["SearchTable"] = SearchTable;
        }
        else
        {
            SearchTable = (DataTable)Session["SearchTable"];
        }
        bool IsSame = false;                                 //表示是否为同一种商品
        foreach (DataRow dr in SearchTable.Rows)                   //遍历DataTable
        {
            if (dr[0].ToString() == "1")
            {
                dr[0] = "1";
                dr[1] = shape;
                dr[2] = price;
                dr[3] = stone;
                dr[4] = color;
                dr[5] = clarity;
                dr[6] = cut;
                IsSame = true;
            }
        }
        if (!IsSame)                                              //DataTable里没有，则创建一行该商品
        {
            drtable = SearchTable.NewRow();
            drtable[0] = "1";
            drtable[1] = shape;
            drtable[2] = price;
            drtable[3] = stone;
            drtable[4] = color;
            drtable[5] = clarity;
            drtable[6] = cut;
            SearchTable.Rows.Add(drtable);
        }
        Session["SearchTable"] = SearchTable;                          //添加到Session中
        Session.Timeout = 1440;
    }
    #endregion
}