﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="integral.aspx.cs" Inherits="shop_integral" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">积分产品</a>
    </header>
    <section class="main main1">
    	<div class="user-admin1">
            <div class="user-admin-info1">
                <a href="/shop/user.aspx" class="user-admin-img"><img src="<%=Application["headimgurl"].ToString()==""? "images/user_pic1.jpg" : Application["headimgurl"].ToString()%>"></a>
                <p>会员卡：<%=Application["usercard"]%><span style=" color:#fff;">可用积分：<%=Application["userscore"]%></span></p>
                <asp:LinkButton ID="LinkButton1" runat="server"  CssClass="qian" OnClick="LinkButton1_Click"  >签到领积分</asp:LinkButton>
               
            </div>
        </div>
        <div class="inte-title">
        	会员积分好礼
        </div>
    	<div class="filter">
        	<ul>
            <li class="current"><a href="/shop/integral.aspx">所有礼品</a></li>
            <asp:Repeater runat="server" ID="RepIntegralColumn">
            <ItemTemplate>
            	<li><a href="/shop/integral.aspx?classid=<%#Eval("classId") %>"><%#Eval("className") %></a></li>
            </ItemTemplate>
            </asp:Repeater>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="inte-product">
        	<ul id="gallery-filtralbe">
            <asp:Repeater ID="RepIntegral" runat="server">
            <ItemTemplate>
            	<li class="br0">
                	<a href="/shop/integralDetail.aspx?id=<%#Eval("articleId") %>">
                    	<img src="<%#Eval("photoUrl") %>" alt="">
                        <h3><%#Eval("title") %></h3>
                        <p><%#getIntegral(Convert.ToInt32(Eval("articleId"))) %>积分<del>¥ <%#getMprice(Convert.ToInt32(Eval("articleId"))) %></del></p>
                    </a>
                </li>
                </ItemTemplate>
                  <%--<FooterTemplate>
                <%#RepIntegral.Items.Count == 0 ? "<div style='text-align:center;'>暂无积分产品</div>" : ""%>
                </FooterTemplate>--%>
                </asp:Repeater>
                <div class="clear"></div>
            </ul>
        </div>
         <input type="hidden" value="<%=classIdList%>" id="oncemore" />
             <a href="javascript:void(0);" class="more-p jia" id="dateTips"></a>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var isajax = true;
            var pageNow = 2;
            var classid = document.getElementById("oncemore").value;
            jQuery(window).scroll(function () {
                if (($(window).scrollTop()) >= ($(document).height() - $(window).height())) {
                    if (isajax) { jQuery(".jia").click(); }
                }
            });
            jQuery(".jia").click(function () {
                if (isajax) {
                    jQuery.ajax(
                    { type: "POST",
                        url: "integralMore.aspx?t=" + Math.random(),
                        data: { "page": pageNow, "classid": classid },
                        dataType: "html",
                        cache: true,
                        beforeSend: function () {
                            jQuery(".jia").addClass("loadboxing");
                            isajax = false;
                        },
                        success: function (data) {
                            if (data != '') {
                                jQuery("#gallery-filtralbe").append(data);
                                isajax = true;
                                pageNow++;
                            }
                            else {
                                isajax = false;
                                $("#dateTips").html("");
                                $("#dateTips").html("暂无更多相关数据");
                            }
                        },
                        error: function () { alert("暂无更多相关数据!"); }
                    })
                }
            });
        });
    </script>
    </form>
</body>
</html>
