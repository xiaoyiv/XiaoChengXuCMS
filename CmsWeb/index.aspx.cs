﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDate();
        }
    }

    #region 赋值操作========================
    public void BindDate()
    {
        DataSet dsRepbanner = new Cms.BLL.C_ad().GetList("adtype=1 and ishidden=0  order by ordernum desc");
        if (dsRepbanner != null && dsRepbanner.Tables[0].Rows.Count > 0)
        {
            Repbanner.DataSource = dsRepbanner.Tables[0].DefaultView;
            Repbanner.DataBind();
            RepbannerI.DataSource = dsRepbanner.Tables[0].DefaultView;
            RepbannerI.DataBind();
        }

        DataSet dsProduct = new Cms.BLL.C_Column().GetList("parentId=94 and ishidden=0  order by classId desc");
        if (dsProduct != null && dsProduct.Tables[0].Rows.Count > 0)
        {
            RepeaterProduct.DataSource = dsProduct.Tables[0].DefaultView;
            RepeaterProduct.DataBind();

        }
        DataSet dsCase = new Cms.BLL.C_article().GetList(8, "parentId=103 and ishidden=0", "articleId desc");
        if (dsCase != null && dsCase.Tables[0].Rows.Count > 0)
        {
            RepeaterCase.DataSource = dsCase.Tables[0].DefaultView;
            RepeaterCase.DataBind();

        }

        DataSet dsNews = new Cms.BLL.C_article().GetList(6, "parentId=107 and ishidden=0", "articleId desc");
        if (dsNews != null && dsNews.Tables[0].Rows.Count > 0)
        {
            RepeaterNew.DataSource = dsNews.Tables[0].DefaultView;
            RepeaterNew.DataBind();

        }

        Cms.Model.C_Column about = new Cms.BLL.C_Column().GetModel(93);
        if (about != null)
        {
            Application["aboutName"] = about.className.ToString();
            Application["subTitle"] = about.sub_title;
            Application["aboutId"] = about.classId;
            Application["aboutBrief"] = about.intro;
        }

       
    }
   
    #endregion
}