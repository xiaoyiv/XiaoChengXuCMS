﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<%@ Register Src="~/html/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="~/html/header.ascx" TagName="header" TagPrefix="uc2" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><%=Application["title"]%></title>
<meta name="Keywords" content="<%=Application["keyword"]%>" />
<meta name="Description" content="<%=Application["Description"]%>" />
<link type="text/css" href="html/css/style.css" rel="stylesheet" />
<script type="text/javascript" src="html/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="html/js/jquery.tool.js"></script>
<script type="text/javascript" src="html/js/focus.js"></script>
</head>

<body>
<uc2:header ID="header1" runat="server" />
<div id="p-top">
  <div id="p-top-banner" class="p-container p-top-qh">
    <div id="p-top-main" class="p-container-body">
      <div class="p-top-qh-pane">
        <div class="p-bannerqh">
           <asp:Repeater runat="server" ID="Repbanner">
          <ItemTemplate>
         
          <div class="p-home-banner <%#Container.ItemIndex==0?"p-selected-view":"p-alternate" %>" id="p-banner-<%#Container.ItemIndex+1%>" style="background-image: url(<%#Eval("picUrl")%>)"> </div>
         
          </ItemTemplate>                
          </asp:Repeater>
        </div>
      </div>
      <div class="p-top-qh-nav">
      <asp:Repeater runat="server" ID="RepbannerI">
      <ItemTemplate>
      <a class="<%#Container.ItemIndex==0?"p-active":"" %>" href="javascript:;"></a>
      </ItemTemplate>                
          </asp:Repeater>
      </div>
    </div>
  </div>
</div>
<div class="indwarp">
  <div class="indmain">
    <div class="indtit">
      <h3>方案主题</h3>
      <p>Solution theme</p>
    </div>
    <ul class="ind-fangan">
      <li><a href="javascript:;" target="_blank"><img src="html/img/img_1.png"><span>苗圃花园</span></a></li>
      <li><a href="javascript:;" target="_blank"><img src="html/img/img_2.png"><span>玫瑰花园</span></a></li>
      <li><a href="javascript:;" target="_blank"><img src="html/img/img_3.png"><span>苗圃花园</span></a></li>
    </ul>
    <div class="indtit mt80">
      <h3><a href="javascript:;">产品系列</a></h3>
      <p>product</p>
    </div>
    <ul class="ind-prodlist">
    <asp:Repeater runat="server" ID="RepeaterProduct">
      <ItemTemplate>
      <li><a href="<%#ToAspx.getCloumnUrl(Convert.ToInt32(Eval("classId")))%>" target="_blank"><%#Eval("className")%></a></li>
      </ItemTemplate>                
      </asp:Repeater>
    </ul>
  </div>
</div>
<div class="indwarp graybg">
  <div class="indmain">
    <div class="indtit">
      <h3>案例展示</h3>
      <p>case</p>
    </div>
    <ul class="ind-case">
    <asp:Repeater runat="server" ID="RepeaterCase">
      <ItemTemplate>
      <li><a href="<%#ToAspx.getContentUrl(Convert.ToInt32(Eval("parentId")),Convert.ToInt32(Eval("articleId")))%>" title="<%#Eval("title")%>" target="_blank" class="ind-c-img"><img src="<%#Eval("photoUrl")%>"></a>
        <p class="ind-c-tit"><a href="<%#ToAspx.getContentUrl(Convert.ToInt32(Eval("parentId")),Convert.ToInt32(Eval("articleId")))%>" title="<%#Eval("title")%>" target="_blank"><%#Eval("title")%></a></p>
      </li>
      </ItemTemplate>                
      </asp:Repeater>
    </ul>
  </div>
</div>
<div class="indwarp">
  <div class="indmain">
    <div class="indleft fl-lt">
      <div class="inlt-tit">
        <h3><a href="javascript:;" target="_blank">新闻资讯</a></h3>
        <p>news</p>
      </div>
      <ul class="ind-clalist">
      <asp:Repeater runat="server" ID="RepeaterNew">
      <ItemTemplate>
        <li><a href="<%#ToAspx.getContentUrl(Convert.ToInt32(Eval("parentId")),Convert.ToInt32(Eval("articleId")))%>" title="<%#Eval("title")%>" target="_blank"><%#Eval("title")%></a></li>
       </ItemTemplate>                
      </asp:Repeater>
      </ul>
    </div>
    <div class="ind-about fl-rt">
      <div class="inlt-tit">
        <h3><a href="javascript:;" target="_blank"><%=Application["aboutName"]%></a></h3>
        <p>ABOUT US</p>
      </div>
      <div class="ind-abox">
        <div class="ind-abimg"><a href="<%=ToAspx.getCloumnUrl(Convert.ToInt32(Application["aboutId"]))%>" target="_blank"><img src="html/img/img_5.png" alt=""></a></div>
        <div class="ind-abinfo">
          <h4><%=Application["subTitle"] %></h4>
          <p><%=Application["aboutBrief"]%><a href="<%=ToAspx.getCloumnUrl(Convert.ToInt32(Application["aboutId"]))%>" target="_blank">查看详细>></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<uc1:footer ID="footer1" runat="server" />
</body>
</html>
