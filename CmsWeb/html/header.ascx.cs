﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class html_header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int classId = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//ID
            int articleId = Convert.ToInt32(this.Request.QueryString["articleId"] ?? "0");//ID
            if (classId > 0)
            { 
            }
            DataSet ds = new Cms.BLL.C_Column().GetList("parentId=26 order by classId");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                RepeaterNav.DataSource = ds.Tables[0].DefaultView;
                RepeaterNav.DataBind();
            }
        }
    }
}