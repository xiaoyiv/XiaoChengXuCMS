﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="product.aspx.cs" Inherits="html_product" %>
<%@ Register Src="~/html/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="~/html/header.ascx" TagName="header" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><%=Application["title"]%></title>
<meta name="Keywords" content="<%=Application["keyword"]%>" />
<meta name="Description" content="<%=Application["Description"]%>" />
<link type="text/css" href="css/style.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.tool.js"></script>
<script type="text/javascript" src="js/focus.js"></script>
</head>

<body>
<form runat="server">
<uc2:header ID="header1" runat="server" />
<!--内页banner-->
<div class="inside-banner"></div>
<div class="indwarp">
  <div class="indmain">
    <dl class="inside-nav">
    <asp:Repeater ID="RepeaterClumn" runat="server" 
            onitemdatabound="RepeaterClumn_ItemDataBound">
    <ItemTemplate>
      <dt><%#Eval("className")%></dt>
      <dd>
      <asp:Repeater ID="RepeaterClumnSub" runat="server">
    <ItemTemplate>
      <a href="<%#ToAspx.getCloumnUrl(Convert.ToInt32(Eval("classId")))%>" target="_blank"><%#Eval("className")%></a>
      </ItemTemplate>
   </asp:Repeater>
      </dd>
    </ItemTemplate>
   </asp:Repeater>  
      
    </dl>
    <div class="inside-rt">
     <div class="inlt-tit">
        <h3><a href="javascript:;" target="_blank"><%=Application["className"]%></a></h3>
        <p><%=Application["engName"]%></p>
      </div>
      <ul class="ind-case">
      <asp:Repeater ID="RepeaterNewlist" runat="server">
                    <ItemTemplate>
        <li><a href="<%#ToAspx.getContentUrl(Convert.ToInt32(Eval("parentId")),Convert.ToInt32(Eval("articleId")))%>" target="_blank" class="ind-c-img"><img src="<%#Eval("photoUrl")%>"></a>
          <p class="ind-c-tit"><a href="<%#ToAspx.getContentUrl(Convert.ToInt32(Eval("parentId")),Convert.ToInt32(Eval("articleId")))%>" target="_blank"><%#Eval("title")%></a></p>
        </li>
       </ItemTemplate>
                    </asp:Repeater>
      </ul>
      <div class="page">
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" HorizontalAlign="Center" FirstPageText="首页"
        LastPageText="尾页" PrevPageText="上一页" NextPageText="下一页" NumericButtonTextFormatString="-{0}-"
        Width="100%" ShowCustomInfoSection="Right"  ShowBoxThreshold="2" 
        PageSize="1" InputBoxClass="text2"
        TextAfterInputBox="" OnPageChanging="AspNetPager1_PageChanging" CssClass="paginator"
        CurrentPageButtonClass="cpb" 
        CustomInfoHTML=" " 
        NumericButtonCount="3" EnableTheming="True" ShowPageIndexBox="Never" CustomInfoSectionWidth="0%" />
    </div>
    </div>
  </div>
</div>
<uc1:footer ID="footer1" runat="server" />
</form>
</body>
</html>

