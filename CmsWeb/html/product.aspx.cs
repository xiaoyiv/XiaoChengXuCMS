﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Cms.DBUtility;

public partial class html_product : System.Web.UI.Page
{
    public DataSet ds;
    public SqlDataAdapter dr;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           int classId = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//ID
            Cms.BLL.C_Column bllcolumn = new Cms.BLL.C_Column();
            if (new Cms.BLL.C_Column().Exists(classId))
            {
                Cms.Model.C_Column model = bllcolumn.GetModel(classId);
                if (model != null)
                {
                    Application["className"] = model.className;
                    Application["content"] = model.content;
                    Application["engName"] = model.engName;
                }
                string where = "select * from C_article where parentId=" + classId + " order by articleId desc";
                this.AspNetPager1.AlwaysShow = true;
                this.AspNetPager1.PageSize = 9;
                this.AspNetPager1.RecordCount = new Cms.BLL.C_article().GetRecordCount("parentId=" + classId + "");
                this.RepeaterDataBind(where);

            }
            else
            {
                Response.Redirect("/index.aspx");
            }
        }

        DataSet ds = new Cms.BLL.C_Column().GetList("parentId=26 order by classId");
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepeaterClumn.DataSource = ds.Tables[0].DefaultView;
            RepeaterClumn.DataBind();
        }
    }

    #region 数据读取===================================================
    public void RepeaterDataBind(string whereStr)
    {
        dr = new SqlDataAdapter(whereStr, DbHelperSQL.connectionString);
        ds = new DataSet();
        dr.Fill(ds, AspNetPager1.PageSize * (AspNetPager1.CurrentPageIndex - 1), AspNetPager1.PageSize, "C_article");
        this.RepeaterNewlist.DataSource = ds.Tables["C_article"];
        this.RepeaterNewlist.DataBind();
    }
    protected void AspNetPager1_PageChanging(object src, Wuqi.Webdiyer.PageChangingEventArgs e)
    {
        int classid = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//栏目ID
        this.AspNetPager1.CurrentPageIndex = e.NewPageIndex;
        string where = "select * from C_article where parentId=" + classid + " order by articleId desc";
        this.RepeaterDataBind(where.ToString());
        
    }
    #endregion
    protected void RepeaterClumn_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepeaterClumnSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int classId = Convert.ToInt32(rowv["classId"]); //获取填充子类的id 
            rep.DataSource = new Cms.BLL.C_Column().GetList("parentId=" + classId + " order by classId").Tables[0].DefaultView;
            rep.DataBind();
        }
    }
}