﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class html_footer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Cms.Model.C_WebSiteconfig site = new Cms.BLL.C_WebSiteconfig().GetModel(1);
        if (site != null)
        {
            Application["Copyright"] = site.Copyright.ToString();
            Application["adress"] = site.adress;
            Application["telphone"] = site.telphone;
            Application["email"] = site.email;
            Application["title"] = site.title;
            Application["keyword"] = site.keyword;
            Application["Description"] = site.Description;
        }
    }
}