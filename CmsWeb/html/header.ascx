﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="html_header" %>
<div class="header">
  <div class="headbox">
    <h1 class="zjyy_logo"><a href="/" target="_blank">兆锦园艺</a></h1>
    <p class="naver">
    <a href="/" target="_blank"  <%if (Request.QueryString["id"]==null&&Request.QueryString["articleId"]==null) {%>class="active"<% } %>>首页</a>
    <asp:Repeater ID="RepeaterNav" runat="server" >
    <ItemTemplate>
    <a href="<%#ToAspx.getCloumnUrl(Convert.ToInt32(Eval("classId")))%>" target="_blank" class="<%#ToAspx.getActive(Request.QueryString["id"],Request.QueryString["articleId"],Convert.ToInt32(Eval("classId")))%>"><%#Eval("className")%></a>
    </ItemTemplate>
   </asp:Repeater>
   
    </p>
  </div>
</div>
